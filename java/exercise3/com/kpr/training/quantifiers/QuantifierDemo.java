/* 
 * Requirements:
 * 		Write a java Program for java regex quantifier.
 * 
 * Entity:
 * 		QuantifierDemo 
 * 
 * Method Signature:
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 		1) Get the input from the user.
 *      2) Declare the regex pattern which type as String.
 *      3) Check Whether the input matches regex pattern,
 *      	3.1) If input match, Print true.
 *          3.2) Otherwise, Print false.
 *          
 * Pseudo Code:
 * class QuantifierDemo {
	
	public static void main(String[] args) {
	    // Get the input from the User
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter input text: ");
		String input = scanner.nextLine();
		//Declare the regex pattern
		// Checks Whether the input matches with the regex pattern using matches
		boolean match = Pattern.matches(regex, input);
		//Print the boolean value
		System.out.println(match);

	}
}
*/

package com.kpr.training.quantifiers;

import java.util.Scanner;
import java.util.regex.Pattern;

public class QuantifierDemo {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter input text: ");
		String input = scanner.nextLine();
		String regex = "[0-7]+?";
		boolean match = Pattern.matches(regex, input);
		System.out.println(match);

	}
}