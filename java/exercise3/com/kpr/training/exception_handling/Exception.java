/*
 * Requirement:
 *     Handle and give the reason for the exception in the following code: 
 *     public class Exception {  
	   public static void main(String[] args) {
       		int arr[] ={1,2,3,4,5};
    		System.out.println(arr[7]);
 	   }
  }	   
 * Entity:
 * 	   Exception
 * 
 * Method Signature:
 *     public static void main(String[] args)
 * 
 * Jobs to be Done:
 *     1)To know whether the code runs or not, we can put try catch exception handling to it
 *     2)In try block , the given code is placed.
 *     3)In catch block, the respective exception is placed.
 *     
 *Pseudo Code:
 *		class Exception {
 *			public static void main(String[] args) {
 *				try
    			{    	
    				int arr[] ={1,2,3,4,5};
    				System.out.println(arr[7]);
    			} catch (Proper excception name) {
    				System.out.println(" The required exception quote");
    			}
 *			}
 *		}
 *     
 */

package com.kpr.training.exception_handling;

public class Exception {  
	
    public static void main(String[] args) {
    	
    try
    {
    	
    	int arr[] ={1,2,3,4,5};
    	System.out.println(arr[7]);
    	
    } catch (ArrayIndexOutOfBoundsException e) {
    	System.out.println(" The array index out of Bounds Exception occurs");
    }
 }
}
/* Answer:
*     The array index is out of range ,so it through a Exception "ArrayIndexOutOfBoundsException"
*/