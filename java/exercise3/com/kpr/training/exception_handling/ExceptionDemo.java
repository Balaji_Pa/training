/*
 * Requirements:
 * To Demonstrate the catching multiple exceptions.
 * 
 * Entities:
 *      ExceptionDemo
 * 
 * Method Signature:
 *    public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *      1) In a try block, Create a object for Scanner.
 *      2) Two integer inputs are get through scanner.
 *      3) Divide the two integer inputs and store it the another integer variable.
 *      4) Create a string variable and assign null as to it..
 *      5) Print the length of the string.
 *          5.1)If the value is divided by zero, then it throws an exception.
 *          5.2)If the length of the string is null, then it throws an exception.        
 *  
 * Pseudo Code:
 *      class ExceptionDemo {
 *          public static void main(String[] args) {
 *              try {
 *                  //input is get from the user as a integer type.
 *                  int number = input1/input2;
 *                  String string = null;
 *                  System.out.println(string.length());
 *              } catch (ArithmeticException exception) {
 *                  System.out.println("Arithmetic Exception occurs");
 *              } catch (NullPointerException exception) {
 *                  System.out.println("Null Pointer Exception occurs");
 *              }
 *          }
 *      }
 */
package com.kpr.training.exception_handling;

import java.util.Scanner;

public class ExceptionDemo {

	public static void main(String[] args) {
		try {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Enter numbers");
			int number1 = scanner.nextInt();
			int number2 = scanner.nextInt();
			int number3 = number1 / number2;
			String string = null;
			System.out.println(string.length());
		} catch (ArithmeticException exception) {
			System.out.println("Arithmetic Exception occurs");
		} catch (NullPointerException exception) {
			System.out.println("NullPointer Exception occurs");
		}
	}

}
