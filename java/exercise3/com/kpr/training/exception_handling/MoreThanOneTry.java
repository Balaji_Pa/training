package com.kpr.training.exception_handling;
/*
* Requirements:
*    It is possible to have more than one try block? - Reason.
* 
* Entities:
*    There is no entity.
*    
* Function Signature:
*    No function is declared.
*
* Solution:
*    No, it cannot possible.
* 
* Explanation:
*     Cannot have multiple try blocks with a single catch block. Each try block must be followed by catch or finally. 
*     Still if we try to have single catch block for multiple try blocks a compile time error is generated.   
*
*/