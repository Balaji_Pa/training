/*
 * Requirements : 
 *      Code for sorting vetor list in descending order
 * Entities :
 *      ReverseList.
 * Function Signature :
 *      public static void main(String[] args)
 * Jobs To Be Done:
 *      1) Create a reference to ArrayList .
 *      2) Add the elements to the arraylist.
 *      3) Invoke the method from collection to sort the ArrayList in descending order.
 *      4) print the arraylist .
 * PSEUDOCODE: 
 *      public class ReverseList {
    
        public static void main(String[] args) {
           ArrayList<Integer> list = new ArrayList<>();
           //add the elements.
           System.out.println("Before Reversing : " + list);
           //sort the arraylist in reverseOrder.
           System.out.println("After Reversing :" + list);
           }
    } 
 */
package com.kpr.training.collections;

import java.util.ArrayList;
import java.util.Collections;

public class ReverseList {
    
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(50);
        list.add(60);
        list.add(70);
        System.out.println("Before Reversing : " + list);

        Collections.sort(list, Collections.reverseOrder());
        System.out.println("After Reversing :" + list);
    }
}
