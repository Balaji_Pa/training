/*
 * Requirement:
 *      8 districts are shown below
 *  Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy  to be converted to UPPERCASE.
   
 * Entity:
 *     Districts
 
 * Method Signature:
 *     public static void main(String[] args)
 
 * Jobs to be done:
 *     1.Add the given districts to the list.
 *     2.List values are replace all to UpperCase and print the list.
 *     
 * Pseudo Code:
 * class Districts {

   		public static void main(String[] args) {
			List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur", "Salem", "Erode", "Trichy");
        	list.replaceAll(String::toUpperCase);
			System.out.println(list);
		}

	}

 */
package com.kpr.training.collections;

import java.util.Arrays;
import java.util.List;

public class Districts {

	public static void main(String[] args) {
		List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur",
				"Salem", "Erode", "Trichy");
		list.replaceAll(String::toUpperCase);
		System.out.println(list);
	}

}