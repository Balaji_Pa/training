/*
 * Requirement:
 *      demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() )
 * Entity:
 *      MapInterface
 * Method Signature:
 *      public static void main(String[] args) 
 * Jobs to be done:
 *     1) Create reference for TreeMap with key as Integer type and value as String type.
 *     2) Add some key-value pairs to the TreeMap reference..
 *     3) Print the TreeMap.
 *     4) Remove a element from the TreeMap.
 *     5) Print the TreeMap.
 *     6) Check whether the TreeMap has a value and print the result.
 *     7) Replace the value of a key.
 *     8) Print the TreeMap.
 * Pseudocode:
 * 
 * public class MapInterface {

    public static void main(String[] args) {
        TreeMap<Integer, String> map = new TreeMap<>();
        //Add the values 
        map.remove(key value which have to remove);
        System.out.println("After removing element the map is " + map);
        System.out.println(map.containsValue(Value which have to remove));
        map.replace(key,value);
        System.out.println("After replacing the key and value of map " + map);
        }
  }
        
 */
package com.kpr.training.collections;

import java.util.TreeMap;

public class MapInterface {

	public static void main(String[] args) {
		TreeMap<Integer, String> map = new TreeMap<>();
		map.put(1, "Karthi");
		map.put(2, "Gokul");
		map.put(3, "Boobalan");
		map.put(4, "Kiruthic");
		map.put(5, "Balaji");
		System.out.println("The map elements are " + map);
		map.remove(5);
		System.out.println("After removing element the map is " + map);
		System.out.println(map.containsValue("Balaji"));
		map.replace(4, "balaji");
		System.out.println("After replacing the key and value of map " + map);
	}
}
