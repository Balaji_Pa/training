/*
 * Requirement:
 *    Addition,Substraction,Multiplication and Division concepts are achieved using Lambda expression and functional interface
 *
 * Entity:
 *    LambdaExample
 *    Arithmetic
 * 
 * Method Signature :
 *   int operation(int a, int b);
 *   public static void main(String[] args)
 *   
 * jobs to done:
 *   1.Add the two integer using lambda expression and print it.
 *   2.Subtract the two integer using lambda expression and print it.
 *   3.Multiply the two integer using lambda expression and print it.
 *   4.Divide the two integer using lambda expression and print it.
 * 
 * Pseudo Code:
 * interface Arithmetic {
   		int operation(int a, int b);
   }
   class LambdaExample {

		public static void main(String[] args) {
            //Using lambda expression and return the Addition value		
			Arithmetic addition = (int a, int b) -> (a + b);
			System.out.println("Addition = " + addition.operation(a, b));
			
			//Using lambda expression and return the Subtraction value 
			Arithmetic subtraction = (int a, int b) -> (a - b);
		    System.out.println("Subtraction = " + subtraction.operation(a, b));
		    
		    //Using lambda expression and return the multiplication value 
		    Arithmetic multiplication = (int a, int b) -> (a * b);
			System.out.println("Multiplication = " + multiplication.operation(a, b));

			//Using lambda expression and return the division value
			Arithmetic division = (int a, int b) -> (a / b);
			System.out.println("Division = " + division.operation(a, b));

		}
	}

*/
package com.kpr.training.collections;

interface Arithmetic {
	int operation(int a, int b);
}


public class LambdaExample {

	public static void main(String[] args) {
		Arithmetic addition = (int a, int b) -> (a + b);
		System.out.println("Addition = " + addition.operation(5, 5));

		Arithmetic subtraction = (int a, int b) -> (a - b);
		System.out.println("Subtraction = " + subtraction.operation(5, 2));

		Arithmetic multiplication = (int a, int b) -> (a * b);
		System.out.println("Multiplication = " + multiplication.operation(5, 6));

		Arithmetic division = (int a, int b) -> (a / b);
		System.out.println("Division = " + division.operation(12, 3));

	}
}