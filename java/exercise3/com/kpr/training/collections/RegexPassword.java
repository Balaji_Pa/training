/*
 * Requirement:
 *       create a pattern for password which contains
 *       8 to 15 characters in length
 *       Must have at least one uppercase letter
 *       Must have at least one lower case letter
 *       Must have at least one digit
 *Entity:
 *     RegexPassword
 * Function Declaration:
 *      public static boolean ValidPassword(String password)
 *      public static void main(String[] args)
 * Jobs to Done:
 *      1) Create package and import Scanner,Matcher and Pattern package  in it.
 *      2) Create class named as RegexPassword and create ValidPassword in it.
 *      3) Give the conditions in the string object
 *      4) By using Pattern compile the regex.
 *      5) Under the main method the password is given at the run time by using scanner.
 *      6) if the password satisfy the given condition it returns true else false. 
 *      
 */
package com.kpr.training.collections;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexPassword {

	public static boolean ValidPassword(String password) {
		String regex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,15}$";

		Pattern pattern = Pattern.compile(regex);
		if (password == null) {
			return false;
		}
		Matcher matcher = pattern.matcher(password);
		return matcher.matches();
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Password ");
		String password = sc.next();
		System.out.println(ValidPassword(password));
		sc.close();
	}
}
