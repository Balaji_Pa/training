/*
 * Requirements:
 * USE OVERRIDE ANNOTATION and complete the program,
  SHOW THE OUTPUT OF THIS PROGRAM: 

class Base 
{ 
     public void display() 
     { 
         
     } 
} 
class Derived extends Base 
{ 
     public void display(int x) 
     { 
         
     } 
  
     public static void main(String args[]) 
     { 
         Derived obj = new Derived(); 
         obj.display(); 
     } 
}
 * Entity:
 *    Base
 *    Derived extends Base
 * 
 * Method Signature:
 *    public void display() 
 *    public void display(int x) 
 *    public static void main(String args[])
 *  
 * Jobs To Be Done:
 *    1) Add the Print statement for display method in Base class and invoke the method.
 *    2) Add override annotation and add print statement in Derived class for the given program and print it. 
 * 
 * PseudoCode:
   class Base {
	public void display() {
		System.out.println("Base class display method is called ");
	}
}


class Derived extends Base {
    @override
	public void display(int x) {
		System.out.println("Derived class display method is called");
	}

	public static void main(String[] args) {
		Derived obj = new Derived();
		obj.display();
	}
}

 */
package com.kpr.training.reflections;

class Base {
	public void display() {
		System.out.println("Base class display method is called ");
	}
}


class Derived extends Base {
//    @Override
	public void display(int x) {
		System.out.println("Derived class display method is called");
	}

	public static void main(String[] args) {
		Derived obj = new Derived();
		obj.display();
	}
}
/* Solution:
	we can't override a method with different number of parameters.
*/