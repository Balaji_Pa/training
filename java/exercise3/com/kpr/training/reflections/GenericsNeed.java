/*

Requirement:
    To explain if the compiler erases all type parameters at compile time, then why should you use generics.
    
Reason:
    1. Java compiler Checks for the type in generic code during compile time.
    2. Generics support programming types as parameters.
    3. Generics enable you to implement generic algorithms.

*/