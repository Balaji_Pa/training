/*
 * Requirement:
 *      To print minimal person with name and email address from the Person class using java.util.Stream<T> API by referring Person.java
 * 
 * Entity:
 *      MinimalPersonList
 *
 * Function Signature:
 *      public static <R, T> void main(String[] args)
 *
 * Jobs To Be Done:
 *      1)Invoke a method createRoster from the class Person and store it to the List.
 *      2)Create a ArrayList of type String to store the name .
 *      3)Create a ArrayList of type String to store the mailId.
 *      4)for each elements in roster list .
 *          4.1)Get the person name and store it in name list .
 *          4.2)Get the person mailId and store it in mailId list .
 *      5)Create minimalName of type String and store the minimal name in name.
 *      6)Create minimalId of type String and store the minimalId in mailId
 *      7)Print the minimalname and minimalId.
 *     
 * Pseudocode:
 * 
 * class MinimalPersonList {
    
    public static <R, T> void main(String[] args) {
        List<Person> roster = Person.createRoster();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> mailId = new ArrayList<>(); 
        for (Person p : roster) {
               //get name 
               //get mailId
        }
        String minimalName = Collections.min(name);
        String minimalId = Collections.min(mailId);

        System.out.println(
                "The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    }

}
        
 */
package com.kpr.training.person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MinimalPersonList {
    
    public static <R, T> void main(String[] args) {
        List<Person> roster = Person.createRoster();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> mailId = new ArrayList<>();
        for (Person p : roster) {
            name.add(p.getName());
            mailId.add(p.getEmailAddress());
        }
        String minimalName = Collections.min(name);
        String minimalId = Collections.min(mailId);

        System.out.println(
                "The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    }

}