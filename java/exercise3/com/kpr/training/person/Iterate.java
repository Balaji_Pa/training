/*
 * Requirement:
 *      Iterate the roster list in Persons class and and print the person without using forLoop/Stream   
 * Entity:
 *      Iterate
 * Function Signature:
 *      public static void main(String[] args)
 * Jobs to be done:
 *      1)Invoke a method createRoster from the class Person and store it to the List.
 *      2)For each elements in the list.
 *          2.1)Print the elements
 * 
 * Pseudocode:
 * 
 * public class Iterate {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Iterator<Person> iterator = roster.iterator();
        while (iterator.hasNext()) {
            //iterate person
            System.out.println(person.name + " " + person.birthday + " " + person.gender + " "
                    + person.emailAddress);
        }
    }
 }
 
 */
package com.kpr.training.person;

import java.util.Iterator;
import java.util.List;

public class Iterate {

	public static void main(String[] args) {
		List<Person> roster = Person.createRoster();
		Iterator<Person> iterator = roster.iterator();
		while (iterator.hasNext()) {
			Person person = iterator.next();
			System.out.println(person.name + " " + person.birthday + " " + person.gender + " "
					+ person.emailAddress);
		}
	}
}
