/*Requirement:
 *  How to read the file contents  line by line in streams with example
 *  
 *Entity:
 *  ReadFile
 *  
 *Function Signature:
 *  public static void main(String[] args)
 *  
 *Jobs to be Done:
 *  1)Create an object for FileInputStream and declare the path of the file .
 *  2)Read the file and store the value in a integer variable.
 *  3)Until the ascii value comes -1 read the file.
 *  4)Print the statement .
 *  
 *Pseudo code:
 *class ReadFile {
    public static void main(String[] args) throws IOException {
        // Declare the file path to read the file
        FileInputStream reader = new FileInputStream("d:\\laptop.txt");

        int data = reader.read();
        while (data != -1) {

            data = reader.read();
            System.out.println(data);
        }
        reader.close();
    }
}
 * 
 */
package com.kpr.training.java_io;

import java.io.FileInputStream;
import java.io.IOException;

public class ReadFile {

	public static void main(String[] args) throws IOException {
		FileInputStream reader = new FileInputStream("laptop.txt");
		int data = reader.read();
		while (data != -1) {

			data = reader.read();
		}
		reader.close();
		System.out.println("The file was read successfully");
	}
}
