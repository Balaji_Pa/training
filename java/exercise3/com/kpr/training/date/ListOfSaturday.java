package com.kpr.training.date;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

/*
Requirement:
    To code for list of all the Saturdays in the current year of the given month.
    
Entity:
    ListOfSaturday
    
Function Declaration:
    public static void main(String[] args) 
    
Jobs to be done:
    1. Get the month from the user for which list of Saturdays to be printed and store it in String
       variable givenMonth.
    2. Create a reference for Month as month that stores the upper case of givenMonth. 
    3. A reference is created for LocalDatae as localDate that stores for the current year and for
       given month the date of first Saturday in that month.
    4. Now create an another instance for Month as newMonth which stores the date of the given month.
    5. Check whether the newMonth and month are equal,
           5.1) if condition satisfies print the date.
                    5.1.1) get the next Saturdays date and print it
                    5.1.2) continue this process until the condition fails. 
           5.2) if condition fails quit the loop.

 Pseudo code:
 class ListOfSaturdays {
     
     public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the month to which list of saturdays to find: ");
        String givenMonth = scanner.nextLine();
        Month month = Month.valueOf(givenMonth.toUpperCase());
        LocalDate localDate = Year.now().atMonth(month).atDay(1)
                              .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        
        Month newMonth = localDate.getMonth();
        while (newMonth == month) {
            System.out.println(localDate);
            localDate = localDate.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            newMonth = localDate.getMonth();
        }
        
        scanner.close();
    }
}
*/

public class ListOfSaturday {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the month to which list of saturdays to find: ");
        String givenMonth = scanner.nextLine();
        Month month = Month.valueOf(givenMonth.toUpperCase());
        System.out.printf("For the month of %s%n", month);
        
        LocalDate localDate = Year.now().atMonth(month).atDay(1)
                              .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        
        Month newMonth = localDate.getMonth();
        while (newMonth == month) {
            System.out.println(localDate);
            localDate = localDate.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            newMonth = localDate.getMonth();
        }
        
        scanner.close();
    }
}
