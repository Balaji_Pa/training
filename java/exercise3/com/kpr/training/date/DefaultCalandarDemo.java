package com.kpr.training.date;

import java.util.Calendar;

/*
Requirement:
    To write a Java program to get and display information (year, month, day, hour, minute) of a 
    default calendar.
    
Entity:
    DefaultCalendarDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. An instance of a Calendar is created as calendar that gets the calendar using current time 
       zone and locale of the system.
    2. Now the information like year, month, day, hour, minute of a default calendar is printed. 
    
Pseudo code:
class DefaultCalandarDemo {
    
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("Year: " + " " + calendar.YEAR);
        System.out.println("Year: " + " " + calendar.MONTH);
        System.out.println("Year: " + " " + calendar.DAY);
        System.out.println("Year: " + " " + calendar.HOUR);
        System.out.println("Year: " + " " + calendar.MINUTE);
    }
}
*/

public class DefaultCalandarDemo {
    
    @SuppressWarnings("static-access")
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("Year: " + " " + calendar.get(calendar.YEAR));
        System.out.println("Month: " + " " + calendar.get(calendar.MONTH));
        System.out.println("Date: " + " " + calendar.get(calendar.DATE));
        System.out.println("Hour: " + " " + calendar.get(calendar.HOUR));
        System.out.println("Minute: " + " " + calendar.get(calendar.MINUTE));
    }
}
