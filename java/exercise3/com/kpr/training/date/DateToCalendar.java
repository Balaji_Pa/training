/*Requirement:
 *  How do you convert a Calendar to Date and vice-versa with example?
 *  
 * Entity:
 *	 DateToCalendar
 *
 * Method Signature:
 *	 public static void main(String[] args)
 * 
 * Jobs To Be Done:
 *    1) Create an object for Date class and constructor parameter is milliseconds.
 *    2) Create a reference for Calendar class.
 *    3) Print the calendar date. 
 * Pseudo Code:
 * class DateToCalendar {

		public static void main(String[] args) {
			Date date = new Date(45373573);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			System.out.println(calendar.getTime());


		}

	}

 *
 */
package com.kpr.training.date;

import java.util.Calendar;
import java.util.Date;

public class DateToCalendar {

	public static void main(String[] args) {
		Date date = new Date(45373573);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println(calendar.getTime());


	}

}
