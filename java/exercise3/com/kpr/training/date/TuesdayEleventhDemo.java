package com.kpr.training.date;

/*
Requirement:
    To Write an example that tests whether a given date occurs on Tuesday the 11th.
    
Entity:
    ThursdayEleventhDemo
    IsTuesdayDemo

Function Declaration:
    public static void main(String[] args
    
Jobs To be Done:
    1.Get the input date from user
    2.Invoke the LocateDate class and pass the Parameter which you get from user 
      2.1) And store it in object of LocalDate's 
      2.2) Use  LocalDate's object and invoke the Class IsTuesdayDemo which implements TemporalQuery<Boolean> 
    3.Check if the user input month is eleven and day is Tuesday
      3.1) if the condition satisfied return true.
      3.2) if the condition not satisfied return false.
      
Pseudo code:
class IsTuesdayDemo implements TemporalQuery<Boolean> {

    public Boolean queryFrom(TemporalAccessor date) {
        return ((date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2));
    }
 }

public class TuesdayEleventhDemo {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the year,month and date as(YYYY-MM-DD)");
        String date = scanner.next();
        LocalDate localDate = LocalDate.parse(date);
        System.out.println(localDate.query(new IsTuesdayDemo()));
        scanner.close();
    }
}
 */

import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQuery;
import java.util.Scanner;

class IsTuesdayDemo implements TemporalQuery<Boolean> {

 // Returns TRUE if the date occurs on Tuesday the 11th.
    public Boolean queryFrom(TemporalAccessor date) {
        return ((date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2));
    }
 }

public class TuesdayEleventhDemo {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the year,month and date as(YYYY-MM-DD)");
        String date = scanner.next();
        LocalDate localDate = LocalDate.parse(date);
        System.out.println(localDate.query(new IsTuesdayDemo()));
        scanner.close();
    }
}
