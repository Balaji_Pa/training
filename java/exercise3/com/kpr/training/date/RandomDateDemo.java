package com.kpr.training.date;

/*
Requirement:
    To find the date of the previous Friday with the given random date.
    
 Entity:
     RandomDateDemo
     
 Function Declaration:
    public static void min(String[] args)
     
Jobs to be done:
    1. The reference is created for the LocalDate as localDate that stores the current date value.
    2. Print the date of the previous friday by taking current date as input.
    
Pseudo code:
class RandomDateDemo {
    
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        System.out.println("The previous Friday is: " + localDate.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
    }
}
*/

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class RandomDateDemo {
    
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        System.out.println("The previous Friday is: " + localDate.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
    }
}
