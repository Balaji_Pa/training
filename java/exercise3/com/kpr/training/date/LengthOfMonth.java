package com.kpr.training.date;

/*
Requirement:
    Write an example that, for a given year, reports the length of each month within that particular year.
    
Entity:
    LengthOfMonth 

Function Declaration:
    public static void main(String[] args);
    
Jobs to be done:
    1. get the user input year and store it in integer variable givenYear.
    2. Now a reference is created for year and integer year is converted into Year format.
    3. Get the month from user and store it as integer.
    4. Now integer month is converted into Month format which is stored in month
    5. Find the length of the month and print it.
    
Pseudo code:
class LengthOfMonth {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the year: ");
        int givenYear = scanner.nextInt();
        Year year = Year.of(givenYear);
        
        System.out.println("Enter the month whose length is to be printed: ");
        int givenMonth = scanner.nextInt();
        Month month = Month.of(givenMonth);
        System.out.println("The length of the given month is: " + month.maxLength());
        scanner.close();
    }
}
*/
import java.util.Scanner;
import java.time.Year;
import java.time.Month;

public class LengthOfMonth {
    
    @SuppressWarnings("unused")
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the year: ");
        int givenYear = scanner.nextInt();
        Year year = Year.of(givenYear);
        
        System.out.println("Enter the month whose length is to be printed: ");
        int givenMonth = scanner.nextInt();
        Month month = Month.of(givenMonth);
        System.out.println("The length of the given month is: " + month.maxLength());
        scanner.close();
    }
}
