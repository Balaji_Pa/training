package com.kpr.training.date;

/*
Requirement:
    To write a java code to print my age
    
Entity:
    AgeDemo
    
Function declaration:
    public static void man(String[] args)
    
Jobs to be done:
    1. Create two instances of LocalDate as dateOfBirth to store your date of birth and age to store
       current date.
    2. Create an instance for Period as myAge and calculate difference between ur dateOfBirth and
       age that is stored in myAge.
    3. Print myAge.
    
Pseudo code:
class AgeDemo {
    
    public static void main(String[] args) {
        LocalDate dateOfBirth = LocalDate.of(2001, 1, 11);
        LocalDate age = LocalDate.now();
        Period myAge = Period.between(dateOfBirth, age);
        System.out.println("I am " + myAge.getYears() + " " + "years old");
    }
}
*/

import java.time.LocalDate;
import java.time.Period;

public class AgeDemo {
    
    public static void main(String[] args) {
        LocalDate dateOfBirth = LocalDate.of(2001, 2, 9);
        LocalDate age = LocalDate.now();
        Period myAge = Period.between(dateOfBirth, age);
        System.out.println("I am " + myAge.getYears() + " " + "years old");
    }
}
