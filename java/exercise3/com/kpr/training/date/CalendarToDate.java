package com.kpr.training.date;

/*
 * Requirement:
 *  How do you convert a Calendar to Date and vice-versa with example?
 *  
 * Entity:
 *	 CalendarToDate 
 *
 * Method Signature:
 *	 public static void main(String[] args)
 * 
 * Jobs To Be Done:
 *    1)Create a reference for Calendar class and also reference is created for Date class.
 *    2)Print the Date with access from Date class and Calendar class.
 * Pseudo Code:
 * class CalendarToDate {

		public static void main(String[] args) {
		    //Access the calendar
			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			System.out.println(date);
		}
  }
 */

import java.util.Date;
import java.util.Calendar;

public class CalendarToDate {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		System.out.println(date);
    }
}
