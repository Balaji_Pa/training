/*
Requirement:
    To write a program to write and read the string to a channel using buffer(to demonstrate Pipe)
    
Entity:
    PipeDemo
    
Function Declaration:
    public static void main(String[] args) throws IOException
    
Jobs to be done:
    1. Create an instance of a pipe.
    2. Create a sink channel.
        2.1 Create a buffer and allocate a space.
        2.2 Add the data to the buffer.
        2.3 Write the data to the sink channel.
    3. Create a source channel.
        3.1 Allocate a space to a buffer.
        3.2 Read the date through source channel.
        3.3 Print the data.
        
Pseudo code:
class PipeDemo {
    public static void main(String[] args) throws IOException {
        
        Pipe pipe = Pipe.open();
               
        Pipe.SinkChannel sinkChannel = pipe.sink();
        
        String testData = "Sentence displayed by reading";
        ByteBuffer buffer = ByteBuffer.allocate(48);
        
       
        while (buffer.hasRemaining()) {
            sinkChannel.write(buffer);
        }
        
        Pipe.SourceChannel sourceChannel = pipe.source();
        buffer = ByteBuffer.allocate(42);
        
            while (buffer.hasRemaining()) {
                char data = (char) buffer.get();
                System.out.print(data);
            }
            
    }
}
*/
package com.kpr.training.java_nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Pipe;

public class PipeDemo {
    public static void main(String[] args) throws IOException {
        // An instance of Pipe is created
        Pipe pipe = Pipe.open();
        // gets the pipe's sink channel
        Pipe.SinkChannel sinkChannel = pipe.sink();
        String testData = "Sentence displayed by reading";
        ByteBuffer buffer = ByteBuffer.allocate(48);
        buffer.clear();
        buffer.put(testData.getBytes());
        buffer.flip();
        // write data into sink channel.
        while (buffer.hasRemaining()) {
            sinkChannel.write(buffer);
        }
        // gets pipe's source channel
        Pipe.SourceChannel sourceChannel = pipe.source();
        buffer = ByteBuffer.allocate(42);
        // write data into console
        while (sourceChannel.read(buffer) > 0) {
            // limit is set to current position and position is set to zero
            buffer.flip();
            while (buffer.hasRemaining()) {
                char data = (char) buffer.get();
                System.out.print(data);
            }
            // position is set to zero and limit is set to capacity to clear the buffer.
            buffer.clear();
        }
    }
}
