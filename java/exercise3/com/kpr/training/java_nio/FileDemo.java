/*
 * Requirement:.
 *    Perform file operations using exists(), createDirectory() and copy() methods
 *Entity:
 *    FileDemo
 *Function Signature:
 *   public static void main(String[] args)
 *Jobs to be done:
 *   1) Create a path instance for a path which has to be created.
 *   2) By using createDirectory method ,path will be created.
 *   3) Create a path instance for a file which has to be created.
 *   4) By using createFile method ,file will be created.
 *   5) Check the file is existing or not
 *      5.1) if it exists,print File exist.
 *      5.2) if it doesn't exist, print File not exist.
 *   6) Create another file by using path instance.
 *   7) By using copy method ,copy a file to another subdirectory
 * 
 * Pseudocode:
 * 
 * class FileDemo {

     public static void main(String[] args) throws IOException {
            //Create path instance name it as path
            Files.createDirectory(path);
            if(Files.exists(sourceFile)) {

                System.out.println("File exists ");
            } 
            else {

                System.out.println("File dos not exists ");
            }
            //Create another path instance name it as path1.
            Files.createDirectory(path1);
            Files.copy(sourceFile,destinationFile);
            System.out.println("File copied successfully ");
            }
     }    
          
 */
package com.kpr.training.java_nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileDemo {

    public static void main(String[] args) throws IOException {

        // creating directory
        Path path = Paths.get("E:/Filedemo/File");
        Files.createDirectory(path);

        // creating a file
        Path sourceFile = Paths.get("E:/Filedemo/File/source.txt");
        Files.createFile(sourceFile);

        // checking the File exists or not
        if (Files.exists(sourceFile)) {

            System.out.println("File exists ");
        } else {

            System.out.println("File dos not exists ");
        }

        Path path1 = Paths.get("E:/Filedemo/File1");
        Files.createDirectory(path1);

        // copying a file to another subdirectory
        Path destinationFile = Paths.get("E:/Filedemo/File1/destination.txt");

        Files.copy(sourceFile, destinationFile);
        System.out.println("File copied successfully ");
    }
}
