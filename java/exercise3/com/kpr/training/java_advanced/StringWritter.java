/*
 * Requirements : 
 * 		Write some String content using Writer.
 *
 * Entities :
 * 		StringWritter
 * Method Signature :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1.Create a reference for FileReader with file as constructor argument.
 *     	2.Till the end of the file
 *          2.1)Read the content of the file.
 *          2.2)Print the content of the file.
 *     	3.Close the created input stream.
 *
 * PseudoCode:
 * 
 * 		class StringWritter {
 *			public static void main(String args[]) throws Exception {
 *				Writer write = new FileWriter("StringWriter.txt");
 *				String content = "I have write a file using writer successfully.";
 *				//Add the content to the file and close it.
 *				System.out.println("success");
 *			}
 *		}
 */
package com.kpr.training.java_advanced;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class StringWritter {

	public static void main(String[] args) throws IOException {
		Writer write = new FileWriter("StringWriter.txt");
		String content = "I have write a file using writer successfully.";
		write.append(content);
		write.close();
		System.out.println("success.");
		
	}

}
