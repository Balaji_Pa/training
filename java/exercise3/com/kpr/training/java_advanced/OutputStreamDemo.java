/*
 * Requirements:
 *     To write some String content using OutputStream
 *    
 * Entities:
 *     OutputStreamDemo
 *     
 * Method Signature:
 *     public static void main(String[] args);
 *     
 * Jobs To Done:
 *     1.Create a reference for FileOutputStream with file as constructor argument.
 *     2.Get the content to be wrote in the file.
 *     3.Convert the given string into input stream 
 *     4.Write the converted content to a file
 *     5.Close the created output stream.
 *
 * Pseudo code:
 *     class OutputStreamDemo {
 *
 *         public static void main(String[] args) {
 *             OutputStream outputStream = new FileOutputStream("outputFile.txt");
 *             String content = "The Java OutputStream class, java.io.OutputStream, is the" +
 *    			         " base class of all output streams in the Java IO API. Subclasses" + 
 *   			         " of OutputStream include the Java BufferedOutputStream and the " +
 *   			         "Java FileOutputStream among others. To see a full list of output" +
 *   			         " streams, go to the bottom table of the Java IO Overview page." ;
 *             //convert the string into input stream
 *             outputStream.write(inputStream);
 *             outputStream.close();
 *         }
 *     }
 */

package com.kpr.training.java_advanced;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamDemo {


    public static void main(String[] args) throws IOException {
        OutputStream outputStream = new FileOutputStream("outputFile.txt");
        String content = "The Java OutputStream class, java.io.OutputStream, is the" +
    			         " base class of all output streams in the Java IO API. Subclasses" + 
    			         " of OutputStream include the Java BufferedOutputStream and the " +
    			         "Java FileOutputStream among others. To see a full list of output" +
    			         " streams, go to the bottom table of the Java IO Overview page." ;
        byte[] inputStream = content.getBytes();
        outputStream.write(inputStream);
        outputStream.close();
        System.out.println("Content has been written successfully.");
    }
}