/*Requirement:
 *      write a program for Java String Regex Methods?
 *      
 *Entity:
 *      RegexMethods
 *      
 *Function Signature:
 *      public static void main(String[] args)
 *      
 *Jobs to be Done:
 *      1)Create a variable of type String and assign the input .
 *      2)Declare the pattern.
 *          2.1)Check if the pattern matches the text and store the result in boolean.
 *          2.2)print the matches.
 *      3)Create a array of type String.
 *          3.1)Split the text using split method and store it on array .
 *      4)for each element in array .
 *          4.1)Print the elements .
 *      
 *      
 *Pseudo code:
 *class RegexMethods {

    public static void main(String[] args) {

        //Get the sentence
        String text =
                "Regular expressions are used for defining String patterns that can be used for searching, manipulating and editing a text";

        String pattern = ".can.";
        boolean matches = Pattern.matches(pattern, text);

        System.out.println("matches = " + matches);

        String[] array = text.split("are");
        for (String string : array) {
            System.out.println(string);
        }


    }
  }
 */

package com.kpr.training.regex;

import java.util.regex.Pattern;

public class RegexMethods {

	public static void main(String[] args) {

		String text =
				"Regular expressions are used for defining String patterns that can be used for searching, manipulating and editing a text.";

		String pattern = ".can.";
		boolean matches = Pattern.matches(pattern, text);

		System.out.println("matches = " + matches);

		String[] array = text.split("are");
		for (String string : array) {
			System.out.println(string);
		}
	}
}
