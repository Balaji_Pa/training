package com.kpr.training.properties;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

public class GreetingsLocale {

	public static void main(String[] args) throws IOException {
		Properties property = new Properties();
		property.put("Greetings", "Welcome");
		
		OutputStream output = new FileOutputStream("C:\\Users\\pabal.BALAJI\\eclipse-workspace\\javaee-demo\\Resource\\Greeting_en.properties");
        property.store(output, "Greetings");
        
        Locale local = new Locale("en", "IN");
        ResourceBundle bundle = ResourceBundle.getBundle("Greeting_en", local);
		System.out.println(bundle.getString("Greetings"));
		
		local = new Locale("fr", "FR");
		bundle = ResourceBundle.getBundle("Greeting_en", local);
		System.out.println(bundle.getString("Greetings"));
		

	}

}
