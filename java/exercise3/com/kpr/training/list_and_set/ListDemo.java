/*
 * Requirements : 
 * 		To do the following:
 * Create a list
 *   => Add 10 values in the list
 *   => Create another list and perform addAll() method with it
 *   => Find the index of some value with indexOf() and lastIndexOf()
 *   => Print the values in the list using 
 *       - For loop
 *      - For Each
 *       - Iterator
 *       - Stream API
 *   => Convert the list to a set
 *   => Convert the list to a array
 *
 * Entities :
 * 		ListDemo.
 * 
 * Method signature :
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 		1) Create an object for ArrayList with type as Integer.
 *      2) Add 10 elements to the list. 
 *      3) Create an object for new ArrayList with type as Integer.
 *      4) Add all the elements from first created list to the new list and print it.
 *      5) Print the first Index value of specific element.
 *      6) Print the last Index value of a specific element.
 *      7) for each element using for loop,
 *      	7.1)Print the list elements.
 *      8) for each element using for each,
 *          8.1)Print the list elements.
 *      9) for each element using iterator,
 *          9.1)Print the list elements.
 *     10) for each element using stream,
 *          10.1)Print the list elements.
 *     11) create an object for HashSet with type as Integer and Convert the list to set and print it.
 *     12) Create an object for Array with Integer type and Convert the list to Array and print it.
 *        
 *          
 * Pseudo code:
 * public class ListDemo {

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        //Add elements to list
        System.out.println(list);

        ArrayList<Integer> newList = new ArrayList<>();
        newList.addAll(list);
        System.out.println("After adding all values to newlist : " + newList);
        System.out.println("Index of a value of element is : " + newList.indexOf(element));
        System.out.println("Last index value of element is : " + newList.lastIndexOf(element));

        // using for loop
        System.out.println("Using for loop :");
        for (int index = 0; index < newList.size(); index++) {
            System.out.print(newList.get(index) + " ");
        }
        System.out.println();

        // using For Each
        System.out.println("Using for each :");
        for (int value : newList) {
            System.out.print(value + " ");
        }
        System.out.println();

        // Using iterator
        System.out.println("using iterator :");
        Iterator<Integer> iterator = newList.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        // Using stream api
        System.out.println("Using Stream :");
        Stream<Integer> stream = newList.stream();
        stream.forEach(value -> System.out.print(value + " "));
        System.out.println();

        // converting list to a set
        Set<Integer> set = new HashSet<>(newList);
        System.out.println("Converted to Set : " + set);

        // converting to a array
        Integer[] array = new Integer[newList.size()];
        array = newList.toArray(array);
        System.out.println("converted to array : " + Arrays.toString(array));
    }

}

 *      
 */
package com.kpr.training.list_and_set;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;

public class ListDemo {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(5);
		list.add(13);
		list.add(4);
		list.add(6);
		list.add(7);
		list.add(4);
		list.add(2);
		list.add(8);
		list.add(4);
		list.add(9);
		System.out.println(list);

		ArrayList<Integer> newList = new ArrayList<>();
		newList.addAll(list);
		System.out.println("After adding all values to newlist : " + newList);
		System.out.println("Index of a value of 4 is : " + newList.indexOf(4));
		System.out.println("Last index value of 4 is : " + newList.lastIndexOf(4));

		// using for loop
		System.out.println("Using for loop :");
		for (int index = 0; index < newList.size(); index++) {
			System.out.print(newList.get(index) + " ");
		}
		System.out.println();

		// using For Each
		System.out.println("Using for each :");
		for (int value : newList) {
			System.out.print(value + " ");
		}
		System.out.println();

		// Using iterator
		System.out.println("using iterator :");
		Iterator<Integer> iterator = newList.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}
		System.out.println();

		// Using stream api
		System.out.println("Using Stream :");
		Stream<Integer> stream = newList.stream();
		stream.forEach(value -> System.out.print(value + " "));
		System.out.println();

		// converting list to a set
		Set<Integer> set = new HashSet<>(newList);
		System.out.println("Converted to Set : " + set);

		// converting to a array
		Integer[] array = new Integer[newList.size()];
		array = newList.toArray(array);
		System.out.println("converted to array : " + Arrays.toString(array));
	}

}
