/*
 * Requirements : 
 * 		To do the following:
 * Create a set
 *  => Add 10 values
 *  => Perform addAll() and removeAll() to the set.
 *  => Iterate the set using 
 *      - Iterator
 *      - for-each
 *
 * Entities :
 * 		SetDemo.
 * 
 * Method Signature:
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 	    1)Create an object for HashSet with type as Integer.
 *      2)Add the elements to the set.
 *      3)Create an object for new HashSet with type as Integer.
 *      4)Add all the elements from first set to the new Set.
 *      5)Remove all the elements in the new Set and print the new Set.
 *      6)for each element using iterator,
 *      	6.1)Print the set elements.
 *      7)for each element using for each,
 *          7.1)Print the set elements.
 *  
 * Pseudo code:
 *		class SetDemo {
		
		    public static void main(String[] args) {
		        HashSet<Integer> set = new HashSet<>();
		        //Add elements to the set
		        HashSet<Integer> newSet = new HashSet<>();
		        newSet.addAll(set);
		        System.out.println("After using addAll method : " + newSet);
		        //Add elements to the set
		        newSet.removeAll(set);
		        System.out.println("After using removeAll method : " + newSet);
		
		        // using Iterator
		        System.out.println("Using Iterator");
		        Iterator<Integer> iterator = set.iterator();
		        while (iterator.hasNext()) {
		            System.out.print(iterator.next() + " ");
		        }
		        System.out.println();
		
		        // using for each
		        System.out.println("Using for-each");
		        for (int values : set) {
		            System.out.print(values + " ");
		        }
		    }
		
		}

 * 		
 */
package com.kpr.training.list_and_set;

import java.util.HashSet;
import java.util.Iterator;

public class SetDemo {

	public static void main(String[] args) {
		HashSet<Integer> set = new HashSet<>();
		set.add(100);
		set.add(200);
		set.add(300);
		set.add(400);
		set.add(500);
		set.add(600);
		set.add(700);
		set.add(800);
		set.add(900);
		set.add(1000);

		HashSet<Integer> newSet = new HashSet<>();
		newSet.addAll(set);
		System.out.println("After using addAll method : " + newSet);
		newSet.add(7000);
		newSet.add(2000);
		newSet.removeAll(set);
		System.out.println("After using removeAll method : " + newSet);

		// using Iterator
		System.out.println("Using Iterator");
		Iterator<Integer> iterator = set.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}
		System.out.println();

		// using for each
		System.out.println("Using for-each");
		for (int values : set) {
			System.out.print(values + " ");
		}
	}

}
