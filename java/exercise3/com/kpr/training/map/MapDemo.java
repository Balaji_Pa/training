/*
 * Requirements:
 *    1) To write a Java program to copy all of the mappings from the specified map to another map?
 *    2) To write a Java program to test if a map contains a mapping for the specified key?
 *    3) Count the size of mappings in a map?
 *    4) To Write a Java program to get the portion of a map whose keys range from a given key to another key?
 
 * Entities:
 *    MapDemo
 
 * Method Signature:
 *    public static void main(String[] args) 
 
 * Jobs To Be Done:
 *    1.Create an object for TreeMap with type as Integer and String.
 *    2.Add the elements in the map which key type as Integer and value type as String.
 *    3.Print the elements of map.
 *    4.Create an object for new TreeMap with Integer type.
 *    5.Add all the elements from First map to Second map.
 *    6.Print the elements of Second map.
 *    7.Print size of the Second map.
 *    8.Print the value of specified key in the second map.
 *    9.Print the range of the element in the Second map.
 *    
 * Pseudo Code:
 *    
 * class MapDemo {
 * 
 * 		public static void main(String[] args) {
 *      	TreeMap<Integer, String> map = new TreeMap<>();
 *      	//Add the elements to the TreeMap
 *      	System.out.println(map);
 *      
 *      	TreeMap<Integer, String> map1 = new TreeMap<>();
 *      	//Add all the elements from first TreeMap to Second TreeMap
 *      
 *      	System.out.println(map1);
 *			System.out.println(map1.size());
 *			System.out.println(map1.get(key));
 *			System.out.println(map1.subMap(start, end));  
 *	 
 *	    }
 *
 * }
 *             
 */
package com.kpr.training.map;

import java.util.TreeMap;

public class MapDemo {

	public static void main(String[] args) {
		TreeMap<Integer, String> map = new TreeMap<>();
		map.put(1, "EEE");
		map.put(2, "ECE");
		map.put(3, "MECH");
		map.put(4, "BME");
		map.put(5, "CHEMICAL");
		System.out.println(map);
		TreeMap<Integer, String> map1 = new TreeMap<>();
		map1.putAll(map);
		System.out.println(map1);
		System.out.println(map1.size());
		System.out.println(map1.get(3));
		System.out.println(map1.subMap(2, 5));
	}

}
