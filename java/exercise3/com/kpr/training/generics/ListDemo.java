/*
 * Requirements : 
 *   To write a program to demonstrate generics - for loop for list.
 *
 * Entities :
 *    ListDemo 
 *    
 * Method Signature :
 *    	public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1)Create a reference for ArrayList of String type.
 *    2)Add some names to the List.
 *    3)Print the first element of the list.
 *    4)For each element in the List.
 *    	4.1)Print the element.
 *    5)For each element in the List.
 *    	5.1)Print the element.
 *      
 * Pseudo Code:
 * Class ListDemo {
 * 
 * 		public static void main(String[] args) {
 *      	ArrayList<String> names = new ArrayList<String>();
 *      	// Declare firstName as string 
 *      	// Add the firstName and other elements to the ArrayList
 *      
 *      	System.out.println(names.get(index));
 *      
 *      	//using iterator
 *      	Iterator<String> stringIterator = names.iterator();
 *			while (stringIterator.hasNext()) {
 *			System.out.println(stringIterator.next());
 *			}
 *
 *      	//using foreach loop
 *			System.out.println("Iterating String list using for loop");
 *			for (String name : names) {
 *				System.out.println(name);
 *      	}
 *	  	}
 * }
 *
 *            
 */
package com.kpr.training.generics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class ListDemo {

	public static void main(String[] args) {
		ArrayList<String> names = new ArrayList<String>();
		String firstName = "Boobalan";
		names.add(firstName);
		names.add("Arya");
		names.add("Surya");
		names.add("Ajith");
		names.add("Ajith");
		System.out.println(names.get(1));
		System.out.println("Iterating String list using while loop");
		Iterator<String> stringIterator = names.iterator();
		while (stringIterator.hasNext()) {
			System.out.println(stringIterator.next());
		}

		System.out.println("Iterating String list using for loop");
		for (String name : names) {
			System.out.println(name);
		}
	}
}
