/*
 * Requirements: 
 *     To write a program to demonstrate generics - class objects as type literals.
 *
 * Entities:
 *     Dog implements Animal
 *    
 * Method Signature:
 *     public static <T> boolean checkInterface(Class<?> theClass);
 *     public void sound();
 *     public static void main(String[] args);
 *    
 * Jobs To Be Done:
 *     1.
 *     
 * Pseudo code:
 * 	   class Dog implements Animal {
 * 
 * 			public boolean checkInterface(Class<?> theClass) {
 * 				return theClass.isInterface();
 * 			}
 * 
 * 			public void sound() {
 * 				System.out.println("Barking");
 * 			}
 * 
 * 			public static void main(String[] args) {
 *       		Class<Integer> intClass = int.class;            
 *        		boolean boolean1 = checkInterface(intClass);
 *      		System.out.println(boolean1);                   
 *      		System.out.println(intClass.getClass());        
 *      		System.out.println(intClass.getName());         
 *
 *      		boolean boolean2 = checkInterface(Dog.class);
 *              System.out.println(boolean2);                   
 *    	        System.out.println(Dog.class.getClass());       
 *		        System.out.println(Dog.class.getName());        
 *		
 *		        boolean boolean3 = checkInterface(Animal.class);
 *		        System.out.println(boolean3);                   
 *		        System.out.println(Animal.class.getClass());    
 *		        System.out.println(Animal.class.getName());     
 *		
 *		        try {
 *		            Class<?> testClass = Class.forName("Dog");
 *		            System.out.println(testClass.getClass());
 *		            System.out.println(testClass.getName());
 *		        } catch (ClassNotFoundException ex) {
 *		            System.out.println(ex.toString());
 *		        }
 *		    }
 *     }     
 */
package com.kpr.training.generics;

public class Dog implements Animal {

	public static <T> boolean checkInterface(Class<?> theClass) {
		return theClass.isInterface();
	}

	public void sound() {
		System.out.println("Barking");
	}

	public static void main(String[] args) {
		Class intClass = int.class; // String.class,Integer.class,Boolean.class
		boolean boolean1 = checkInterface(intClass);
		System.out.println(boolean1); // prints false
		System.out.println(intClass.getClass()); // prints java.lang.Class
		System.out.println(intClass.getName()); // prints int

		boolean boolean2 = checkInterface(Dog.class);
		System.out.println(boolean2); // prints false
		System.out.println(Dog.class.getClass()); // prints java.lang.Class
		System.out.println(Dog.class.getName()); // prints Dog

		boolean boolean3 = checkInterface(Animal.class);
		System.out.println(boolean3); // prints true
		System.out.println(Animal.class.getClass()); // prints java.lang.Class
		System.out.println(Animal.class.getName()); // prints Animal

		try {
			Class testClass = Class.forName("Dog");
			System.out.println(testClass.getClass());
			System.out.println(testClass.getName());
		} catch (ClassNotFoundException ex) {
			System.out.println(ex.toString());
		}
	}
}
