/*
 * Requirements : 
 *   To write a program to demonstrate generics - for loop for map.
 *
 * Entities :
 *    Map 
 *    
 * Method Signature :
 *     public static <K, E> void printMap(HashMap<K, E> map)
 *     public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *     1)Create a reference of HashMap with Key type as Integer and value type as String.
 *     2)Create a scanner object.
 *     3)Get integer input for map size from user.
 *     4)Iterate from 0 to range.
 *         4.1)Get key, value pair as input  from user and add it to the HashMap.
 *     5)Call the method to print the HashMap elements.
 *         5.1)Iterate through the keySet.
 *             5.1.1)Store the value corresponding to the key in a variable.
 *             5.1.2)Print the key and value.
 *  
 * Pseudocode:
 * class MapDemo {

	//method to print the HashMap
	public static <K, E> void printMap(HashMap<K, E> map) {
		for (K key : map.keySet()) {
			E value = map.get(key);
			System.out.println(key + " : " + value);
		}
	}

	public static void main(String[] args) {
		HashMap<Integer, String> firstMap = new HashMap<Integer, String>();
		Scanner scanner = new Scanner(System.in);
		
		//add elements to firstMap for the given range
		int range = scanner.nextInt();
		for (int element = 0; element < range; element++) {
			firstMap.put(scanner.nextInt(), scanner.nextLine());
		}
		
		//Print the firstMap by calling the method printMap.
		printMap(firstMap);
		scanner.close();
	}
}
 */
package com.kpr.training.generics;

import java.util.HashMap;
import java.util.Scanner;

public class MapDemo {

	public static <K, E> void printMap(HashMap<K, E> map) {
		for (K key : map.keySet()) {
			E value = map.get(key);
			System.out.println(key + " : " + value);
		}
	}

	public static void main(String[] args) {
		HashMap<Integer, String> firstMap = new HashMap<Integer, String>();
		Scanner scanner = new Scanner(System.in);
		int range = scanner.nextInt();
		scanner.hasNext();
		for (int element = 0; element < range; element++) {
			int integer = scanner.nextInt();
			String string = scanner.nextLine();
			firstMap.put(integer, string);
		}
		printMap(firstMap);
		scanner.close();
	}
}
