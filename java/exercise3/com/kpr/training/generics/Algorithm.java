/*
 * Requirements : 
 * 		Will the following class compile? If not, why?
 *  	public final class Algorithm {
 *          public static <T> T max(T x, T y) {
 *              return x > y ? x : y;
 *          }
 *      }
 *
 * Entities :
 * 		Algorithm.
 * Method Signature :
 * 		public static <T> T max(T x, T y)
 * Jobs To Be Done:
 * 		1)Check whether the given code compile or not.
 */

/*
 * The code does not compile, because the greater than (>) operator applies only to primitive numeric types.
 */
package com.kpr.training.generics;

public class Algorithm {

	public static <T> T  max(T x, T y) {
        return (x > y) ? x : y;
    }
}
