package com.kpr.training.multi_threading;
/*
 * Requirements:
 *	   What are the arguments passed for the sleep() method?
 *
 * Entity:
 *     There is no Entity.
 * 
 * Method Signature:
 *     No Method Signature.
 *    
 * Jobs To Be Done:
 *     1.Find out which method is passed for the sleep() method in multithreading.
 *     
 * Solution:
 *     We can pass long type milliseconds and integer type nanoseconds in sleep() method.
 *     
 */