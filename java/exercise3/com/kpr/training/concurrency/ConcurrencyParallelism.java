/*
 *Requirement:
 *    Explain in words Concurrency and Parallelism(in two sentence)
 *Explanation:
  Concurrency:
     * Concurrency is the ability to run several programs or several parts of a program in parallel.
     * If a time consuming task can be performed asynchronously or in parallel, this improves the throughput and the interactivity of the      program.

  Parallelism:
     * Parallelism does not require two tasks to exist. It literally physically run parts of tasks OR multiple tasks, at the same time  
  using multi-core infrastructure of CPU, by assigning one core to each task or sub-task.
     * Parallelism requires hardware with multiple processing units, essentially. In single core CPU, you may get concurrency but NOTparallelism.
*/