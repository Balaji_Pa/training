package com.kpr.training.concurrency;
/*
 * Requirements : 
 * 		 Write a program of performing two tasks by two threads that implements Runnable interface.
 * Entities :
 * 		Task1
 * 		Task2
 * 		TwoThread
 * Function Signature :
 * 		public void runTask1()
 * 		public void runTask2()
 * 		public static void main(String[] args).
 * Jobs To Be Done:
 * 		1)Create object for Runnable interface and define the method for task1.
 * 		2)Create object for Runnable interface and define the method for task2.
 * 		3)Create objects for Thread for both task1 and task2.
 * 		4)Start both process.
 * pseudo code:
 * 
 * 		public class TwoThread {
			public static void main(String args[]) {
				//Creating object of runnable interface and defining run method
				Runnable runTask1 = new Runnable() {
					public void run() {
						System.out.println("task one");
					}
				};
				
				//Creating object of runnable interface and defining run method
				Runnable runTask2 = new Runnable() {
					public void run() {
						System.out.println("task two");
					}
				};
				
				//Creating object of Thread
				Thread task1 = new Thread(runTask1);
				Thread task2 = new Thread(runTask2);
			
				//Starting the process
				task1.start();
				task2.start();
			}
		} 
 */
public class TwoThread {
	public static void main(String args[]) {
		Runnable runTask1 = new Runnable() {
			public void run() {
				System.out.println("task one completed");
			}
		};

		Runnable runTask2 = new Runnable() {
			public void run() {
				System.out.println("task two completed");
			}
		};

		Thread task1 = new Thread(runTask1);
		Thread task2 = new Thread(runTask2);

		task1.start();
		task2.start();
	}
} 
 