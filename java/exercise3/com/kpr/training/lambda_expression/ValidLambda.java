package com.kpr.training.lambda_expression;
/*
 * Requirement:
 *
    To Find what is wrong with the following interface? and fix it.
        (int x, y) -> x + y;

Entities:
    No entities.

Function Declaration:
    No function is declared.

Jobs To be Done:
    1)Checking the valid expression.


Solution:
    In the given expression, The data type identifier must be given to both variables otherwise both variables doesn't have data type identifier

Correct Expression:
    (int x, int y) -> x + y; or (x, y) -> x + y;
 */