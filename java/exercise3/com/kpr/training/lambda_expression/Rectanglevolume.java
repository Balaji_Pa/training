/*
 * Requirements : 
 *      Write a program to print the volume of  a Rectangle using lambda expression
 * Entities :
 *      interface AreaOfRectangle,
 *      public class RectangleVolume.
 * Function Signature :
 *      double area(double num1, double num2),
 *      public static void main(String[] args).
 * Jobs To Be Done:
 *      1)Get the input from user as length and breadth.
 *      2)Multiple the length and breadth using lambda expression.
 *      3)Print the rectangle area
 *      
 *  Pseudo code:
 *  interface AreaOfRectangle {

        double area(double num1, double num2);
    }


    class Rectanglevolume {

        public static void main(String[] args) {
            System.out.println("Enter the number");
            Scanner scanner = new Scanner(System.in);
            double length = scanner.nextDouble();
            double breadth = scanner.nextDouble();
            //Multiple the two value using lambda expression
            System.out.println(rectangleArea.area(length, breadth));

        }

    }    
 */
package com.kpr.training.lambda_expression;

import java.util.Scanner;
import java.lang.FunctionalInterface;

@FunctionalInterface
interface AreaOfRectangle {

	double area(double num1, double num2);
}


public class Rectanglevolume {

	public static void main(String[] args) {
		System.out.println("Enter the number");
		Scanner scanner = new Scanner(System.in);
		double length = scanner.nextDouble();
		double breadth = scanner.nextDouble();
		AreaOfRectangle rectangleArea = (num1, num2) -> num1 * num2;
		System.out.println(rectangleArea.area(length, breadth));

	}

}
