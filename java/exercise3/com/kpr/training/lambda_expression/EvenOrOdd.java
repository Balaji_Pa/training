/*
 * Requirements : 
 * 		 To convert the following anonymous class into lambda expression.
 *  interface CheckNumber {
	    public boolean isEven(int value);
	}
	
	public class EvenOrOdd {
	    public static void main(String[] args) {
	        CheckNumber number = new CheckNumber() {
	            public boolean isEven(int value) {
	                if (value % 2 == 0) {
	                    return true;
	                } else return false;
	            }
	        };
	        System.out.println(number.isEven(11));
	    }
	}
 * Entities :
 * 		CheckNumber
 * 		EvenOrOdd
 * 
 * Method Signature :
 * 		boolean isEven(int number);
 * 		public static void main(String[] args).
 * 
 * Jobs To Be Done:
 * 		1)Get input from the user.
 * 		2)To check odd or even, the input is modulo divided by 2,
 * 			2.1)if the remainder is zero, return even.
 * 			2.2)If the remainder is not zero, return odd.
 * 		3)Print the returned output.
 * Pseudocode:
 * 
 * interface CheckNumber {
 *     boolean isEven(int number);
 * }
 * public class EvenOrOdd{
 *     public static void main(String[] args) {
 *         Scanner scanner = new Scanner(System.in);
 *         int numberInput = scanner.nextInt();
 *         CheckNumber check = (number) -> (number % 2 ==0) ? true : false;
 *         System.out.println(check.isEven(numberInput));
 *     }
 * }
 */
package com.kpr.training.lambda_expression;

import java.util.Scanner;

interface CheckNumber {

	boolean isEven(int number);
}

public class EvenOrOdd {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int numberInput = scanner.nextInt();
		CheckNumber check = (number) -> (number % 2 == 0) ? true : false;
		System.out.println(check.isEven(numberInput));
	}
}
