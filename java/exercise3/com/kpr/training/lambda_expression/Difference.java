/*
 * Requirements : 
 * 		 write a program to print difference of two numbers using lambda expression 
 *  and the single method interface
 *  
 * Entities :
 * 		MyInterface,
 * 		Difference.
 * 
 * Method Signature :
 * 		int difference(int one, int two),
 * 		public static void main(String[] args).
 * 
 * Jobs To Be Done:
 * 		1)Get the input for two numbers.
 * 		2)Both the inputs are subtracted and printed.
 * 
 * Pseudocode:
 * 
 * interface Subtraction {
 * 	  int difference(int one, int two);
 * }
 * class Difference {
 * 	  public static void main(String[] args) {
 * 
 * 		  //getting two integer input from user.
 * 		  Scanner scanner = new Scanner(System.in);
 * 		  int number1 = scanner.nextInt();
 * 		  int number2 = scanner.nextInt();
 * 
 * 		  //lambda expression to find and return the difference.
 * 		  Subtraction test = (input1,input2) -> input - input2;
 * 		  System.out.println(test.difference(number1, number2));
 */
package com.kpr.training.lambda_expression;

import java.util.Scanner;

interface Subtraction {

	int difference(int one, int two);
}

public class Difference {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int number1 = scanner.nextInt();
		int number2 = scanner.nextInt();
		Subtraction test = (input1, input2) -> input1 - input2;
		System.out.println(test.difference(number1, number2));
	}
}