/*
Requirement:
    To print the sum of the variable arguments.

Entity:
    public class LambdaVariableArguments

Method Signature:
    public int printAddition(int ... numbers)
    public static void main(String[] args)
    
Jobs to be done:
     1) Declare a lambda expression which takes integer array as argument.
     2) For each elements in the array,
     	2.1)Add each values of the array
     3)Print the Final sum of the array elements.
     
Pseudo code:
interface VariableArgumentDemo {

    public int printAddition(int... numbers);
    
}


class LambdaVariableArguments {

    public static void main(String[] args) {
        VariableArgumentDemo value = (numbers) -> {
            int sum = 0;
            //Iterate and add all the elements
            for (int values : numbers) {
                sum = sum + values;
            }

            return sum;
        };

        System.out.println("The sum of elements is: " + value.printAddition(elements));
    }
}


 */
package com.kpr.training.lambda_expression;

interface VariableArgumentDemo {

	public int printAddition(int... numbers);
}


public class LambdaVariableArguments {

	public static void main(String[] args) {
		VariableArgumentDemo value = (numbers) -> {
			int sum = 0;

			for (int values : numbers) {
				sum = sum + values;
			}

			return sum;
		};

		System.out.println("The sum of elements is: " + value.printAddition(1, 5, 6, 7));
	}
}