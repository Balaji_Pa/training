package training;

import test.Abstract;

public class TestAbstractPackage extends Abstract {

	public void printName() {
		System.out.println("Balaji");
	}

	private void printAge() {
		System.out.println("20");
	}

	protected void printNationality() {
		System.out.println("Indian");
	}

	public static void main(String[] args) {
		TestAbstractPackage test = new TestAbstractPackage();
		test.printName();
		test.printAge();
		test.printNationality();

	}

}
