package training;

public class DifferentPackageClass {
	public void printName() {
		System.out.println("Balaji");
	}

	private void printAge() {
		System.out.println("20");
	}

	protected void printNationality() {
		System.out.println("Indian");
	}
}
