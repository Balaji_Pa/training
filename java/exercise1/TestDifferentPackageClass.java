package test;

import training.DifferentPackageClass;

public class TestDifferentPackageClass {

	public static void main(String[] args) {
		DifferentPackageClass test = new DifferentPackageClass();
		test.printName();
		test.printAge();			//The method printAge() from the type DifferentPackageClass is not visible
		test.printNationality();	//The method printNationality() from the type DifferentPackageClass is not visible
	}

}
