package test;

public class TestInterface implements Interface {

	public void printName() {
		System.out.println("Balaji");
	}

	private void printAge() {
		System.out.println("20");
	}

	protected void printNationality() {
		System.out.println("Indian");
	}

	public static void main(String[] args) {
		TestInterface test = new TestInterface();
		test.printName();
		test.printAge();
		test.printNationality();
	}
}
