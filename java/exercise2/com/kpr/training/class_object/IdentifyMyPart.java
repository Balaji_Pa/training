package com.kpr.training.class_object;
/*
Requirements:
    To find the output of the given program.
    IdentifyMyParts a = new IdentifyMyParts();
    IdentifyMyParts b = new IdentifyMyParts();
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
    System.out.println("a.y = " + a.y);
    System.out.println("b.y = " + b.y);
    System.out.println("a.x = " + a.x);
    System.out.println("b.x = " + b.x);
    System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);

Entities:
    public class IdentifyMyParts

Method Signature:
    public static void main(String[] args)

Jobs To Be Done:
    1)Finding the values of class variable x and instance y variables for both the objects a and b
*/
public class IdentifyMyPart {

  public static void main(String[] args) {
    IdentifyMyParts a = new IdentifyMyParts();
    IdentifyMyParts b = new IdentifyMyParts();
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
    System.out.println("a.y = " + a.y);
    System.out.println("b.y = " + b.y);
    System.out.println("a.x = " + a.x);
    System.out.println("b.x = " + b.x);
    System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);
  }
}
/*OUTPUT:
  a.y = 5
  b.y = 6
  a.x = 2
  b.x = 2
  IdentifyMyParts.x = 2
 */