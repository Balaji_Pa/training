/*
 * Requirement: 
 * 		To find what's wrong with the following program? And fix it.
 * 
 * 		public class SomethingIsWrong { 
 * 			public static void main(String[] args) { 
 * 				Rectangle myRect;
 * 				myRect.width = 40; 		
 * 				myRect.height = 50; 
 * 				System.out.println("myRect's area is " + myRect.area()); 
 * 			}
 * 		}
 * 
 * Entities: 
 * 		Rectangle
 * 
 * Method Signature: 
 * 		public int area() 
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done: 
 * 		1) Declare two integer variables.
 * 		2) Define a method.
 * 			2.1)return the product of two above declared variables.
 * 		3)Create a object of this class.
 * 		4)assign the values of the two declared variables.
 * 		5)Print the area by invoking the method.
 */
package com.kpr.training.class_object;

// #THE CORRECTED PROGRAM:
public class Rectangle {
	
	public int width;
	public int height;

	public int area() {
		return (width * height);
	}

	public static void main(String[] args) {
		Rectangle myRect = new Rectangle();
		myRect.width = 40;
		myRect.height = 50;
		System.out.println("myRect's area is " + myRect.area());
	}
}

