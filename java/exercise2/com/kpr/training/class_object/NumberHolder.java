package com.kpr.training.class_object;

/*
 Requirements:
    To write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.

    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }
 Entities: 
 	NumberHolder
  
 Method Signature: 
 	public static void main(String[] args)
  
 Jobs To Be Done: 
 	1)Create a object of the class.
 	2)Assign the values of two declared variables.
 	3)Print the values of both the variables.
 
 */
public class NumberHolder {
	public int anInt;
	public float aFloat;

	public static void main(String[] args) {
		NumberHolder numberHolder = new NumberHolder();
		numberHolder.anInt = 100;
		numberHolder.aFloat = 22.67f;
		System.out.println(numberHolder.anInt);
		System.out.println(numberHolder.aFloat);
	}
}
