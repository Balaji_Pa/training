package com.kpr.training.class_object;
/*
Requirements:
    To find the class and instance Variables in the given program.
    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }

Entities:
    IdentifyMyParts

Method Signature:
    No method signature.

Jobs To Be Done:
    1)Declare a static integer variable x and assign the value 7.
    2)Declare a integer variable y and assign the value 3.
*/
public class IdentifyMyParts {

  public static int x = 7;
  public int y = 3;
}

//Class Variable : x     //because the variable is declared with static keyword.
//Instance Variable : y
