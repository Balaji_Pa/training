package com.kpr.training.controlFlow_exercise;

/*
 * Requirement: 
 * 		To find fibonacci series using Recursion.
 * 
 * Entity: 
 * 		FibonacciRecursion
 * 
 * Method Signature: 
 * 		public static void printSeries() 
 * 		public static void main(String[] args)
 * 
 * Jobs to be Done: 
 * 		1)Declare three integer variable number1, number2 and sum.
 * 		2)Create a method which accepts one integer parameter.
 * 			2.1)check whether the parameter value is greater than 0.
 * 				2.1.1)add number1 and number2 and assign it to sum.
 * 				2.1.2)assign the value of number1 to number2.
 * 				2.1.3)assign the value of sum to number2.
 * 				2.1.4)Print the number1 and a white space.
 * 				2.1.5)invoke this method with parameter value minus 1 as parameter.
 * 		3)Declare a integer variable rangeValue.
 * 		4)Assign 0 to number1 and 1 to number2.
 * 		5)Create a Scanner object.
 * 		6)Get the integer input from user and assign it to rangeValue.
 * 		7)invoke the above defined method with rangeValue as parameter.
 */

import java.util.Scanner;

public class FibonacciRecursion {

	public static int number1;
	public static int number2;
	public static int sum;

	public static void printSeries(int finalValue) {
		if (finalValue > 0) {
			sum = number1 + number2;
			number1 = number2;
			number2 = sum;
			System.out.print(number1 + " ");
			printSeries(finalValue - 1);
		}
	}

	public static void main(String[] args) {
		int rangeValue;
		number1 = 0;
		number2 = 1;
		Scanner scanner = new Scanner(System.in);
		rangeValue = scanner.nextInt();
		printSeries(rangeValue);
	}
}
