package com.kpr.training.controlFlow_exercise;

/*
 * Requirement: 
 * 		To find fibonacci series using while loop.
 * 
 * Entity: 
 * 		FibonacciWhile
 * 
 * Method Signature: 
 * 		public static void main(String[] args).
 * 
 * Jobs To Be Done: 
 * 		1)Declare two integer variable rangeValue and sum.
 * 		2)Declare a integer variable number1 and assign 0 to it.
 * 		3)Declare a integer variable number2 and assign 1 to it.
 * 		4)Declare a integer variable initialValue and assign 1 to it.
 * 		5)Create a Scanner object.
 * 		6)Get the integer input from user and assign it to rangeValue.
 * 		7)check whether the initialValue is less than or equal to rangeValue.
 * 			7.1)if true, then
 * 				7.1.1)add number1 and number2 and assign it to sum.
 * 				7.1.2)assign the value of number2 to number1.
 * 				7.1.3)assign the value of sum to number2.
 * 				7.1.4)Increment the value of initialValue.
 * 				7.1.5)Print number1 and white space.
 */

import java.util.Scanner;

public class FibonacciWhile {

	public static void main(String[] args) {
		int rangeValue;
		int number1 = 0;
		int number2 = 1;
		int initialValue = 1;
		int sum;
		Scanner scanner = new Scanner(System.in);
		rangeValue = scanner.nextInt();
		while (initialValue <= rangeValue) {
			sum = number1 + number2;
			number1 = number2;
			number2 = sum;
			initialValue++;
			System.out.print(number1 + " ");
		}
	}
}
