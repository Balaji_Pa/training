/*
Requirement;
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects.
    
Entities:
    Snake extends Animal

Function Signature:
    public void sound().

Jobs to be done:
    1)Declare the method sound.
        1.1)print the statement.
 */

package com.kpr.training.inheritance;

public class Snake extends Animal {
    
    public void sound() {
        System.out.println("The snake crawls");
    }
}