package com.kpr.training.inheritance;
/*
Requirement:
    To consider the following classes and answer the following questions.
    public class ClassA {
        public void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public static void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }

    public class ClassB extends ClassA {
        public static void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }
    a. Which method overrides a method in the superclass?
    b. Which method hides a method in the superclass?
    c. What do the other methods do?


Entity:
    public class ClassA
    public class ClassB extends ClassA

Function Declaation:
    public void methodOne(int i)
    public void methodTwo(int i)
    public void methodThree(int i)
    public static void methodFour(int i)

Jobs To Be Done:
    1)Looking the given program.
    2)Explaining the answers for the given questions.
*/
public class ClassA {
	public void methodOne(int i) {}

	public void methodTwo(int i) {}

	public static void methodThree(int i) {}

	public static void methodFour(int i) {}
}


public class ClassB extends ClassA {
	public static void methodOne(int i) {} //methodOne shows a compile time error as 'Non static method' cannot call the Static method.

	public void methodTwo(int i) {} //The method methodTwo overrides a method in the superclass(public void methodTwo(int i)).

	public void methodThree(int i) {}  //methodThree shows a compile time error as 'Non static method
    cannot call the Static method.

	public static void methodFour(int i) {} //The method methodFour hides a method in the super class.(public static void methodFour).
}
