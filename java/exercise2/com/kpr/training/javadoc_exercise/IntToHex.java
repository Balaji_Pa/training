/*Requirement:
    To find Integer method can you use to convert an int into a string that expresses the number in
    hexadecimal.

Entity:
    IntegerMethodDemo.

Function Signature:
    public static void main(String[] args)

Jobs to be Done:
    1. Create integer variable and assign the integer value to it.
    2. Print the hexadecimal value of the given integer value.
*/
package com.kpr.training.javadoc_exercise;

public class IntToHex {

    public static void main(String[] args) {
        int number = 75;
        // returns the string representation of the integer value in HexaDecimal format
        System.out.println("HexaDecimal representation is = " + Integer.toHexString(number).toUpperCase());
    }
}