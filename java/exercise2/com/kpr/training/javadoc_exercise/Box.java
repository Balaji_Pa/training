package com.kpr.training.javadoc_exercise;
/*Requirement:
    To use the Java API documentation for the Box class (in the javax.swing package) to help to
    answer the following questions.
    1. What static nested class does Box define?
    2. What inner class does Box define?
    3. What is the superclass of Box's inner class?
    4. Which of Box's nested classes can you use from any class?
    5. How do you create an instance of Box's Filler class?

Answers:
1. Box.Filler.
2. Box.AccessibleBox.
3. [java.awt.]Container.AccessibleAWTContainer.
4. Box.Filler.
5. new Box.Filler(minDimension, prefDimension, maxDimension)
 */