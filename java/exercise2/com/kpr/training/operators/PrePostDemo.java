/*Requirement:
    To explain why the value "6" is printed twice in a row:
       class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }
Entities:
    PrePostDemo

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1)Explain the reason why the "6" is printed twice in a row.
*/
/* 
Explanation:
    ++i increments the value of i by 1 and returns the i value.
	i++ returns the value of i and increments the value by 1.
*/