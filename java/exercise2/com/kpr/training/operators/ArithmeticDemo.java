/*
Requirement:
    To change the given program in the form of compound assignments.
    class ArithmeticDemo {

            public static void main (String[] args){

                 int result = 1 + 2; // result is now 3
                 System.out.println(result);

                 result = result - 1; // result is now 2
                 System.out.println(result);

                 result = result * 2; // result is now 4
                 System.out.println(result);

                 result = result / 2; // result is now 2
                 System.out.println(result);

                 result = result + 8; // result is now 10
                 result = result % 7; // result is now 3
                 System.out.println(result);
            }
       }
    
Entity:
    ArithmeticDemo

Method Signature:
    public static void main (String[] args)
    
Jobs to be done:
    1.Add 1 and 2 using assignment operator and assigned to the integer variable and print it.
    2.Subtract 1 from the previous integer value using Compound assignments and print it.
    3.Multiply 2 from the previous integer value using Compound assignments and print it.
    4.Divide by 2 from the previous integer value using Compound assignments and print it.
    5.Add 8 from the previous integer value using Compound assignments and print it.
    6.Modulo divide by 7 from the previous integer value using Compound assignments and print it.



*/
package com.kpr.training.operators;

public class ArithmeticDemo {

	public static void main(String[] args) {
		int result = 1 + 2; // result is now 3
		System.out.println(result);

		result -= 1; // result is now 2
		System.out.println(result);

		result *= 2; // result is now 4
		System.out.println(result);

		result /= 2; // result is now 2
		System.out.println(result);

		result += 8; // result is now 10
		System.out.println(result);

		result %= 7; // result is now 3
		System.out.println(result);

	}
}
