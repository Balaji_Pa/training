/*
 * Requirement: 
 * 		To print the absolute path of the .class file of the current class.
 * 
 * Entity: 
 * 		CurrentDirectory.
 * 
 * Function Declaration: 
 * 		public static void main(String[] args).
 * 
 * Jobs to be Done: 
 * 		1)Get the path of the .class file and store it in the String variable.
 * 		2)Print the value stored in the String.
 */

package com.kpr.training.javalang_exercise;

public class ClassDirectory {

	public static void main(String[] args) {
		String path = System.getProperty("user.dir");
		System.out.println("File Directory = " + path);
	}
}

