/*
Requirement:
    Create a program that reads an unspecified number of integer arguments from the command line and adds them together.
    For example, suppose that you enter the following: java Adder 1 3 2 10
    The program should display 16 and then exit. The program should display an error message if the user enters only one argument.
    

Entity:
    AdderDemo
    
Functoin Declaration:
    public static void main(String[] args)
    public static void add(int... array)
    
Jobs To Be Done:
    1) Assign the given values as array and store it to array variable.
    2) Call the method and pass the array.
    3) Check if array length is "1" 
           3.1) if yes,prints Add more new numbers.
           3.2) otherwise, initialize the sum is equal to zero.
           3.3) Add the each element in the array and store it to the sum.
           3.4) print the array.
*/
    
package com.kpr.training.datatype_exercise;

public class IntAdder {

    public static void add(int array[]) {
        if (array.length == 1) {
            System.out.println("Add more new numbers");
        } else {
            int sum = 0;
            
            for (int number : array) {
                sum = sum + number;
            }
            System.out.println(sum);
        }
    }

    public static void main(String[] args) {
        int array[] = {1, 3, 2, 10};
        add(array);
    }
}


