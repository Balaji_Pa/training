/*
Requirement:
    To invert the value of a boolean, which operator would you use?

Entity:
    BooleanDemo

Function Signature:
     public static void main(String[] args) 

Jobs To Be Done:
    1) Assign a boolean expression to the boolean variable.
    2) print the inverted value of a boolean variable.
    
Answer:
    The logical complement operator "!" would be used
*/

package com.kpr.training.datatype_exercise;

public class BooleanDemo {

    public static void main(String[] args) {
        boolean value = (10 == 8);
        System.out.println("Result: " + !value);
    }
}
