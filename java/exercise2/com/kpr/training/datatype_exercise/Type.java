/*Requirement:
  print the type of the result value of following expressions
  - 100 / 24
  - 100.10 / 10
  - 'Z' / 2
  - 10.5 / 0.5
  - 12.4 % 5.5
  - 100 % 56
Entity:
    DataType
Function Signature:
    public static void main(String[] args)
Jobs to be done:
    1) Create object variable and assign the given expression.
    2) print the type of the result value of the object  variable.
    
*/    
package com.kpr.training.datatype_exercise;

public class Type {

	public static void main(String[] args) {
		Object number1 = 100 / 24;
		System.out.println(number1.getClass().getName());
		Object number2 = 100.10 / 10;
		System.out.println(number2.getClass().getName());
		Object number3 = 'Z' / 2;
		System.out.println(number3.getClass().getName());
		Object number4 = 10.5 / 0.5;
		System.out.println(number4.getClass().getName());
		Object number5 = 12.4 % 5.5;
		System.out.println(number5.getClass().getName());
		Object number6 = 100 / 56;
		System.out.println(number6.getClass().getName());
	}
}
