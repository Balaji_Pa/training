/*
 * Requirement:
 * 		To demonstrate the abstract class using the class shape that has two methods to
 * calculate the area and perimeter of two classes named the Square and the Circle extended from the
 * class Shape.
 *
 * Entity:
 * 		Shape
 *
 * Method Signature:
 * 		public void printArea();
 *		public void print Perimeter()
 *
 * Jobs To Be Done:
 * 		1)Declare the abstract method printArea and
 * 		  printPerimeter.
 */
package com.kpr.training.abstractDemo;

public abstract class Shape {

	public abstract void printArea();

	public abstract void printPerimeter();
}
