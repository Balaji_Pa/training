/*
Requirement:
     To implements the CharSequence interface found in the java.lang package and returning the string backwards.

Entity:
    public class CharSequenceDemo implements CharSequence

Function Declaration:
    public char charAt(int a),
    public int length(),
    public void subSequence(int a, int b),
    public static void main(String[] args).

Jobs To Be Done:
    1) The String value is assigned to the variable.
    2)In java Lang package it has mainly four packages
    	2.1)By using length function, the length of the string is analysed.
    	2.2)By using charAt function, the position of the characters is found through index.
    	2.3) By Substring, The String is split into Various substring.
    	2.4) For toString, the content is converted into  string.
    3)For each character in the Given string
    	3.1)Each of the characters are reversed.
*/
package interface_exercise;

import java.util.Arrays;

public class CharSequenceDemo implements CharSequence{
    public static String sentence;
    public char charAt(int a){
        return sentence.charAt(a);
    }
    public int length(){
        return sentence.length();
    }
    public String subSequence(int a, int b){
        return (String) sentence.subSequence(a,b);
    }
    public static void main(String[] args){
        CharSequenceDemo words = new CharSequenceDemo();
        sentence = "This program return the CharSequence interface found in the java.lang.package";
        char[] reversed = new char[words.length()];
        for(int index = words.length() - 1; index >=0; index--) {
        	if(index == words.length() - 1) {
        		reversed[index - (words.length() - 1)] = sentence.charAt(index);
        	}else {
        		reversed[-(index - (words.length() - 1))] = sentence.charAt(index);
        	}
        }
        System.out.println(sentence);
        System.out.println(Arrays.toString(reversed).replace(", ", "").replace("[", "").replace("]", ""));
    }
}