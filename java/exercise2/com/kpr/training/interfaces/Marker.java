/*
Requirement:
    To analyze the following interface valid or not:
    public interface Marker {}

Entity:
    public interface Marker.

Function Declaration:
    Here there is no function declaration.

Jobs To Be Done:
    1.answering the question.
 */
package com.kpr.training.interfaces;

public interface Marker {

}
/*
 * yes,the interface is valid
 */
