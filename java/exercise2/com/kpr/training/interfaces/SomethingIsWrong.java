package com.kpr.training.interfaces;

/*
Requirement:
    To identify what is wrong in the given interface program.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }

Entity:
    SomethingIsWrong 

Function Signature:
    void aMethod(int aValue) 

Jobs to be done:
    Look at the given program , identify the error and then Correct the program.

Explanation:
Default and static variables can implements have a method implementations.so the correct format is
*/
public interface SomethingIsWrong {
    static void aMethod(int aValue){     // Here default or static can be used.
        System.out.println("Hi Mom");
    }
}