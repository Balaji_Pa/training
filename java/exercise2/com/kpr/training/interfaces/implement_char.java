/*
Requirement:
    What methods would a class that implements the java.lang.CharSequence interface have to implement
    
Solution:
    1.charAt-Return the character at the particular index.
    2.length - Return the length of the string.
    3.subSequence - Return the subsequence of the particular sequence.4.toString - Return the string representation of object.

*/
package com.kpr.training.interfaces;

public class implement_char implements CharSequence {

	public int length() {

		return 0;
	}

	public char charAt(int index) {
		return 0;
	}

	public CharSequence subSequence(int start, int end) {

		return null;
	}
}
