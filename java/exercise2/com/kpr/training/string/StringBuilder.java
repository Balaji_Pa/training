package com.kpr.training.string;
/* 
Requirement:
    To find the initial capacity of the following string builder:
    StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.")

Entity:
    There is no Entity.

Function Signature:
    There is no function declared.
    
Jobs To Be Done:
    1.Find the length of the given string and Add the value 16 in it.
    2.Find the initial capacity of the given String.
*/
/*Solution:
    length of the given String: 26
    initial capacity of the String: 26 + 16 = 42
 */