/*
Requirement:
    To find the two ways to concatenate the string "Hi, mom."
    String hi = "Hi, ";
    String mom = "mom.";
 
Entities:
     There is no entity.
    
Function Signature :
     No,Function Declared.
     
Jobs To Be Done:
    1.Concatenate the two given string values by using concat method .
    2.Concatenate the two given string values by using Addition operator.
 */
// SOLUTION:
package com.kpr.training.string;

public class Concat {

	public static void main(String[] args) {
		String hi = "Hi, ";
		String mom = "mom.";
		System.out.println(hi + mom);
		System.out.println(hi.concat(mom));
	}
}
