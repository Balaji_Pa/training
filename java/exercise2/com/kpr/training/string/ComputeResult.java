/*Requirement:
To find the output of the following program after each numbered line.
		String original = "software";
        StringBuilder result = new StringBuilder("hi");
        int index = original.indexOf('a');

  		System.out.println(result.setCharAt(0, original.charAt(0)));  		                     //1
	   	System.out.println(result.setCharAt(1, original.charAt(original.length()-1)));           //2
	   	System.out.println(result.insert(1, original.charAt(4)));								 //3
	  	System.out.println(result.append(original.substring(1,4)));								 //4
	  	System.out.println(result.insert(3, (original.substring(index, index+2) + " ")));		 //5

        System.out.println(result);

Entity:
ComputeResult

Method Signature:
public static void main(String[] args)

Jobs to be done:
1) A string value is initialized.
2) Another string builder is assigned.
3) The given conditions are checked and printed.

*/
package com.kpr.training.string;

public class ComputeResult {

	public static void main(String[] args) {
		String original = "software";
		StringBuilder newString = new StringBuilder("hi");
		int index = original.indexOf('a');
		/* 1 */ newString.setCharAt(0, original.charAt(0));
		System.out.println(newString);
		/* 2 */ newString.setCharAt(1, original.charAt(original.length() - 1));
		System.out.println(newString);
		/* 3 */ newString.insert(1, original.charAt(4));
		System.out.println(newString);
		/* 4 */ newString.append(original.substring(1, 4));
		System.out.println(newString);
		/* 5 */ newString.insert(3, (original.substring(index, index + 2) + " "));
		System.out.println(newString);
	}
}
/*
OUTPUT:

1)si
2)se
3)swe 
4)sweoft
5)swear oft
Swear oft
*/