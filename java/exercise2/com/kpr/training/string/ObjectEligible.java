/*Requirement:
    To analyze the following code and to check the number of reference to those objects exist after the code executes.And also
    checking whether the object is eligible or garbage collection.
    
    String[] students = new String[10];
    String studentName = "Peter Parker";
    students[0] = studentName;
    studentName = null;

Entity:
    ObjectEligible. 
    
Function Signature:
    public static void main(String[] args)

Jobs to be done:
    1.Analyze and Check Whether the object is eligible or garbage collection.
    2.Explain the reason.
    

*/
package com.kpr.training.string;

public class ObjectEligible {

	public static void main(String[] args) {
        String[] students = new String[10];
        String studentName = "Peter Parker";
        students[0] = studentName;
        System.out.println(students[0]);
        studentName = null;
        System.out.println(students);
    }
}

/*
 * ANSWER: 1)Here the array has 1 reference and it is "Peter Parker". 2)It is neither eligible nor
 * garbage collection. Because the studentName is assigned as null, so there is no value is
 * initialized after this. 3)But the array size is 10 and its has only one reference value.
 */
