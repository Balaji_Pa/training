/*Requirement:
    To sort and print following String[] alphabetically ignoring case. Also convert and print even indexed Strings into uppercase
       { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }

Entity:
    City

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1) To print the given city names in sorted order, the city names are assigned to the String list.
    2) The list is sorted and printed.
    3)For each city name, 
    	3.1)Check if the city name is present in even index.
    		3.1.1)If yes, convert that particular city into UpperCase.
    		

*/
package com.kpr.training.string;

import java.util.*;

public class Sort {

	public static void main(String[] args) {
		String[] cityName = {"Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem"};
		Arrays.sort(cityName);
		System.out.println("Sorted: " + Arrays.toString(cityName));
		for (int i = 0; i < cityName.length; i++) {
			if (i % 2 == 0) {
				cityName[i] = cityName[i].toUpperCase();
			}
		}
		System.out.println("Case converted: " + Arrays.toString(cityName));
	}
}
