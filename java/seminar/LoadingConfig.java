package seminar;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoadingConfig {

	public static void main(String[] args) {
		Properties properties = new Properties();
		
		//Loading the properties file.
		try(InputStream input = new FileInputStream("Capitals.properties")){
		    properties.load(input);
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		//Printing the properties.
		properties.list(System.out);
		
	}
}
