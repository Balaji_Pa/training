package seminar;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class StoringConfigXML {

	public static void main(String[] args) {
		Properties properties = new Properties();
		//setting some property as (key - value) pair
		properties.setProperty("India", "New Delhi");
		properties.setProperty("Pakistan", "Islamabad");
		properties.setProperty("Sri Lanka", "Colombo");
		properties.setProperty("Nepal", "Kathmandu");
		
		//storing the properties to file.
		try(OutputStream output = new FileOutputStream("Capitals.xml")){
		    properties.storeToXML(output, "These are capitals.");
		    System.out.println("Properties stored successful");
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}

}
