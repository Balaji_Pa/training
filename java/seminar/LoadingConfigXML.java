package seminar;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoadingConfigXML {

	public static void main(String[] args) {
		Properties properties = new Properties();
		
		//Loading the properties from XML file.
		try(InputStream input = new FileInputStream("Capitals.xml")){
		    properties.loadFromXML(input);
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		//Printing the properties.
		properties.list(System.out);
		
	}
}
