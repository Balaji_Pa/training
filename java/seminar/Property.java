package seminar;

import java.util.Iterator;
import java.util.Properties;

public class Property {
	public static void main(String[] args) {
		Properties properties = new Properties();
		//setting some property as (key - value) pair
		properties.setProperty("user_name", "balaji@2001");
		properties.setProperty("password", "9201@balaji");
		properties.setProperty("email", "balajieee2022@gmail.com");
		properties.setProperty("mobile_number", "8124416117");
		properties.setProperty("name", "ashok");
		
		//getting a property and store it a string.
		String email = properties.getProperty("email");
		System.out.println(email);
		
		//getting a property with default value.
		System.out.println();
		String name = properties.getProperty("name", "balaji");
		System.out.println(name);
		
		//iterating properties
		System.out.println();
		Iterator<Object> iterator = properties.keySet().iterator();
		while(iterator.hasNext()) {
			String key = (String) iterator.next();
			String value = properties.getProperty(key);
			System.out.println(key + " = " + value);
		}
		
		//removing a property.
		System.out.println();
		properties.remove("email");
		System.out.println("After removing a property : " + properties);
	}
}
