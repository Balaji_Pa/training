package seminar;

import java.util.Properties;

public class DefaultProperties {

	public static void main(String[] args) {
		// creating a default properties
		Properties defaultProperties = new Properties();
		defaultProperties.setProperty("Bangladesh", "Dhaka");
		defaultProperties.setProperty("Bhutan", "Thimphu");
		defaultProperties.setProperty("Myanmar", "Naypyitaw");

		Properties properties = new Properties(defaultProperties);
		// setting some property as (key - value) pair
		properties.setProperty("India", "New Delhi");
		properties.setProperty("Pakistan", "Islamabad");
		properties.setProperty("Sri Lanka", "Colombo");
		properties.setProperty("Nepal", "Kathmandu");

		System.out.println(properties.getProperty("Bhutan"));
		properties.list(System.out);
		
	}
}
