/*
 * Requirement:
 * 		To create a POJO class for Address.
 * 
 * Entity:
 * 		Address
 * 
 * Method Signature:
 * 		public Address(String street, String city, int postal_code)
 * 		public String getStreet()
 * 		public void setStreet(String street)
 * 		public String getCity()
 * 		public void setCity(String city)
 * 		public String getPostalCode()
 * 		public void setPostalCode(int postal_code)
 *      public String toString()
 * 
 * Jobs to be Done:
 * 		1) Declare the fields as private.
 * 		2) Create a constructor with four parameters which is the values of fields.
 * 			2.1)Increment the id value and assign the value to id.
 * 			2.2)Assign the values of fields with argument value.
 * 		3) Create the method to get the id. 
 * 		4) create the method to get the street.
 * 		5) Create the method to get the city.
 * 		6) Create the method to get the Postal Code.
 * 		7) Create the method to assign the value of id. 
 * 		8) create the method to assign the value of street.
 * 		9) Create the method to assign the value of city.
 * 		10) Create the method to assign the value of postal code.
 *      11) Override the toString method.
 * 
 * Pseudo Code:
 * 		class Address {
 * 			
 * 			public long id;
			public String street;
			public String city;
			public int postal_code;
			
			public Address(String street, String city, int postal_code) {
				this.id = id++;
				this.street = street;
				this.city = city;
				this.postalCode = postal_code;
			}
			
			public String getStreet() {
				return street;
			}
			public void setStreet(String street) {
				this.street = street;
			}
			public String getCity() {
				return city;
			}
			public void setCity(String city) {
				this.city = city;
			}
			public int getPostal_code() {
				return postalCode;
			}
			public void setPostalCode(int postal_code) {
				this.postal_code = postal_code;
			}
			public long getId() {
				return id;
			}
			
			@Override
            public String toString() {
                return "Address [id=" + id + ", street=" + street + ", city=" + city + ", postalCode="
                        + postalCode + "]";
            }
 * 		}
 */
package com.kpr.training.jdbc.model;

public class Address {

	private long id;
	private String street;
	private String city;
	private int postalCode;

    public Address(String street, String city, int postal_code) {

        this.street = street;
        this.city = city;
        this.postalCode = postal_code;
    }

    public String getStreet() {
        return street;
    }

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	@Override
    public String toString() {
	    
        if (this.id == 0 && this.postalCode == 0) {
            return "null";
        } else {
            return new StringBuilder("Address [id=").append(id).append(", street=").append(street)
                    .append(", city=").append(city).append(", postalCode=").append(postalCode)
                    .append("]").toString();
        }
        
    }
}
