@startuml
entity ConnectionService
ConnectionService -> commit
activate commit
entity Connection
group try
Connection -> Connection.commit : commit the changes
end
group catch
entity AppException
commit -> AppException : Catch Exception
end
deactivate commit
@enduml