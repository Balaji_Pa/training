@startuml
entity ConnectionService
ConnectionService -> get
get -> ConnectionService : return Connection
@enduml