package com.kpr.training.jdbc.exception;

public enum ErrorCode {

	INVALID_POSTAL_CODE("ERR401", "postal code should not be zero"),
	READING_ADDRESS_FAILED("ERR402", "Failed to read address"),
	ADDRESS_CREATION_FAILED("ERR403", "Failed to create Address"),
	ADDRESS_UPDATION_FAILED("ERR404", "Failed to update Address"),
	ADDRESS_DELETION_FAILED("ERR405", "Failed to delete Address"),                              
	PERSON_CREATION_FAILED("ERR406", "Failed to create Person"),                                 
	PERSON_UPDATION_FAILED("ERR407", "Failed to update Person"),                                
	PERSON_DELETION_FAILED("ERR408", "Failed to delete Person"),                               
	CONNECTION_FAILED("ERR409", "Failed to initiate connection"),                               
	EMAIL_NOT_UNIQUE("ERR411", "Email already exists"),                                      
	CONNECTION_FAILED_TO_CLOSE("ERR412", "Failed to close the connection"),                     
	FAILED_TO_COMMIT_OR_ROLLBACK("ERR413", "Failed to commit or rollback"),                                        
	FAILED_TO_CHECK_EMAIL("ERR415", "Failed to check email"),                                  
	READING_PERSON_FAILED("ERR416", "Failed to read person"),                                   
	READING_ADDRESSID_FAILED("ERR417", "Failed to read address id"),                            
	FAILED_TO_CHECK_ADDRESS("ERR418", "Failed to check address"),                              
	SEARCHING_ADDRESS_FAILED("ERR419", "Failed to search address"),             
	FIRST_NAME_AND_LAST_NAME_DUPLICATE("ERR421", "First name and last name already exists"), 
	CHECKING_ADDRESS_USAGE_FAILED("ERR422", "Failed to check address usage"),                   
	WRONG_DATE_FORMAT("ERR423", "Enter valid date in valid format dd-mm-yyyy"),                
	CHECKING_UNIQUE_NAME_FAILED("ERR424", "Failed to check name is unique or not"),             
	SETTING_VALUE_FAILED("ERR425", "Failed to set value for Prepared Statement"),                  
	INVALID_FIRST_NAME("ERR428", "First name should not be null or empty"),                      
	INVALID_LAST_NAME("ERR429", "Last name should not be null or empty"),                        
	INVALID_EMAIL("ERR430", "Email should not be null or empty");                                
   
    private final String code;
    private final String message;

    ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
