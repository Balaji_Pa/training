package com.kpr.training.jdbc.json.handler;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtil {

public Object jsonToObject(String json, Object typeObj) {
             
        return new GsonBuilder().setDateFormat("yyyy-MM-dd").create().fromJson(json, typeObj.getClass());   
    }
    
    public String objectToJson(Object object) {
        
        return new Gson().toJson(object);
    }
}