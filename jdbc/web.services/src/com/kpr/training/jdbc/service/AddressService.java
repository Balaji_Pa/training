/*
 * Requirement: To perform the CRUD operation of the Address.
 * 
 * Entity: 1.Address 2.AddressService 3.AppException 4.ErrorCode
 * 
 * Function declaration: public long create(Address address) {} public Address read(long id) {}
 * public ArrayList<Address> readAll() {} public void update(Address address) {} public void
 * delete(long id) {} public ArrayList<Address> search(String street, String city, String
 * postalCode) {} public Address readAddress(ResultSet result) {} Jobs To Be Done: 1. Create a
 * Address. 2. Read a record in the Address. 3. Read all the record in the addresses. 4. Update an
 * Address. 5. Delete an Address. 6. Search the address which contains given data. 7. Perform common
 * operations for read and readAll.
 */

package com.kpr.training.jdbc.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;

public class AddressService {

    public long create(Address address) {

        validateAddress(address);
        
        try (PreparedStatement ps = ConnectionService.get().prepareStatement(
                QueryStatement.CREATE_ADDRESS_QUERY, PreparedStatement.RETURN_GENERATED_KEYS)) {

            preparePS(ps, address);
            ResultSet resultSet;

            if ((ps.executeUpdate() != 1) || !(resultSet = ps.getGeneratedKeys()).next()) {
                throw new AppException(ErrorCode.ADDRESS_CREATION_FAILED);
            }
            return resultSet.getLong(Constant.GENERATED_ID);
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_CREATION_FAILED, e);
        }
    }

    public Address read(long id) {

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.READ_ADDRESS_QUERY)) {

            ps.setLong(1, id);
            ResultSet resultSet;

            if ((resultSet = ps.executeQuery()).next()) {
                return prepareAddress(resultSet);
            }
            return null;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILED, e);
        }
    }

    public ArrayList<Address> readAll() {

        ArrayList<Address> addresses = new ArrayList<>();

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.READALL_ADDRESS_QUERY)) {

            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                addresses.add(prepareAddress(resultSet));
            }

            return addresses;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILED, e);
        }
    }

    public void update(Address address) {

        validateAddress(address);

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.UPDATE_ADDRESS_QUERY)) {
            preparePS(ps, address);
            ps.setLong(4, address.getId());
            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.ADDRESS_UPDATION_FAILED);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_UPDATION_FAILED, e);
        }
    }

    public void delete(long id) {

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.DELETE_ADDRESS_QUERY)) {

            ps.setLong(1, id);

            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.ADDRESS_DELETION_FAILED);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_DELETION_FAILED, e);
        }
    }

    public ArrayList<Address> search(String street, String city, String postalCode) {

        ArrayList<Address> addresses = new ArrayList<>();

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.ADDRESS_SEARCH)) {

            ps.setString(1, street);
            ps.setString(2, city);
            ps.setString(3, postalCode);
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                addresses.add(prepareAddress(resultSet));
            }
            return addresses;
        } catch (Exception e) {
            throw new AppException(ErrorCode.SEARCHING_ADDRESS_FAILED, e);
        }
    }

    public Address prepareAddress(ResultSet resultSet) {
        
        Address address;
        
        try {
            address = new Address(resultSet.getString(Constant.STREET),
                    resultSet.getString(Constant.CITY), resultSet.getInt(Constant.POSTAL_CODE));
            address.setId(resultSet.getLong(Constant.ID));
            return address;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILED, e);
        }
    }

    public void preparePS(PreparedStatement ps, Address address) {

        try {
            ps.setString(1, address.getStreet());
            ps.setString(2, address.getCity());
            ps.setInt(3, address.getPostalCode());
        } catch (Exception e) {
            throw new AppException(ErrorCode.SETTING_VALUE_FAILED, e);
        }
    }

    public long getAddressIdForAddress(Address address) {

        ResultSet resultSet;

        try (PreparedStatement ps = ConnectionService.get()
                .prepareStatement(QueryStatement.GET_ADDRESS_ID_FROM_ADDRESS)) {

            preparePS(ps, address);

            if ((resultSet = ps.executeQuery()).next()) {
                return resultSet.getLong(Constant.ID);
            }
            return 0;
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_CHECK_ADDRESS, e);
        }
    }

    public void validateAddress(Address address) {

        if (address.getPostalCode() == 0) {
            throw new AppException(ErrorCode.INVALID_POSTAL_CODE);
        }
    }

}
