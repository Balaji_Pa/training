package com.kpr.training.jdbc.service;

/*
Requirement:
    To perform the CRUD operation of the Person.
 
Entity:
    1.Person
    2.PersonService
    3.AppException
    4.ErrorCode
 
Function declaration:
    public long create(Person person, Address address)
    public Person read(long id, boolean addressFlag)
    public ArrayList<Person> readAll()
    public void update(long id, Person person, Address address)
    public void delete(long id)

Jobs To Be Done:
    1. Create a Person.
    2. Read a record in the Person.
    3. Read all the record in the Person.
    4. Update a Person.
    5. Delete a Person.
*/

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;

public class PersonService {

    public long create(Person person) {

        validatePerson(person);
        checkNameIsUnique(person, ConnectionService.get());
        checkEmailIsUnique(person.getEmail(), ConnectionService.get());

        try (PreparedStatement ps = ConnectionService.get().prepareStatement(
                QueryStatement.CREATE_PERSON_QUERY, PreparedStatement.RETURN_GENERATED_KEYS)) {

            Address address = person.getAddress();
            address.setId(assignAddressId(person));
            person.setAddress(address);
            preparePS(ps, person);
            ResultSet resultSet;
            if (ps.executeUpdate() != 1 || !((resultSet = ps.getGeneratedKeys()).next())) {
                throw new AppException(ErrorCode.PERSON_CREATION_FAILED);
            }
            return resultSet.getLong(Constant.GENERATED_ID);
        } catch (Exception e) {
            throw new AppException(ErrorCode.PERSON_CREATION_FAILED, e);
        }
    }

    public Person read(long id, boolean addressFlag) {

        Person person = null;
        AddressService addressService = new AddressService();

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.READ_PERSON_QUERY)) {

            ps.setLong(1, id);
            ResultSet resultSet;
            
            if ((resultSet = ps.executeQuery()).next()) {
                person = preparePerson(resultSet);
                if (addressFlag) {
                    person.setAddress(addressService.read(resultSet.getLong(Constant.ADDRESS_ID)));
                }
            }
            return person;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_PERSON_FAILED, e);
        }
    }

    public ArrayList<Person> readAll() {

        ArrayList<Person> persons = new ArrayList<>();
        AddressService addressService = new AddressService();
        Person person;

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.READALL_PERSON_QUERY)) {
            ResultSet resultSet;
            resultSet = ps.executeQuery();

            while (resultSet.next()) {
                person = preparePerson(resultSet);
                person.setAddress(addressService.read(resultSet.getLong(Constant.ADDRESS_ID)));
                persons.add(person);
            }
            return persons;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_PERSON_FAILED, e);
        }
    }

    public void update(Person person) {

        validatePerson(person);
        checkNameIsUnique(person.getId(), person, ConnectionService.get());
        checkEmailIsUnique(person.getId(), person.getEmail(), ConnectionService.get());

        try (PreparedStatement ps = ConnectionService.get()
                .prepareStatement(QueryStatement.UPDATE_PERSON_QUERY.toString())) {

            Address address = person.getAddress();
            address.setId(assignUpdateAddressId(person));
            person.setAddress(address);
            preparePS(ps, person);
            ps.setLong(6, person.getId());

            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.PERSON_UPDATION_FAILED);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.PERSON_UPDATION_FAILED, e);
        }

    }

    public void delete(long id) {

        boolean delete = false;
        long addressId = getAddressIdFromPersonId(id, ConnectionService.get());

        if (addressId != 0) {

            if (getAddressUsage(addressId, ConnectionService.get()) != true) {
                delete = true;
            }
        }

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.DELETE_PERSON_QUERY)) {
            ps.setLong(1, id);

            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.PERSON_DELETION_FAILED);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.PERSON_DELETION_FAILED, e);
        }

        if (delete) {
            AddressService addressService = new AddressService();
            addressService.delete(addressId);
        }
    }


    public void checkEmailIsUnique(long id, String email, Connection con) {

        ResultSet resultSet;
        long personId = 0;

        try (PreparedStatement ps = con.prepareStatement(QueryStatement.EMAIL_UNIQUE)) {
            ps.setString(1, email);
            
            if ((resultSet = ps.executeQuery()).next()) {
                personId = resultSet.getLong(Constant.ID);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_CHECK_EMAIL, e);
        }

        if ((personId > 0 && personId != id)) {
            throw new AppException(ErrorCode.EMAIL_NOT_UNIQUE);
        }
    }
    
    public void checkEmailIsUnique(String email, Connection con) {

        ResultSet resultSet;
        long personId = 0;

        try (PreparedStatement ps = con.prepareStatement(QueryStatement.EMAIL_UNIQUE)) {
            ps.setString(1, email);

            if ((resultSet = ps.executeQuery()).next()) {
                personId = resultSet.getLong(Constant.ID);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_CHECK_EMAIL, e);
        }

        if ((personId > 0)) {
            throw new AppException(ErrorCode.EMAIL_NOT_UNIQUE);
        }
    }

    public long getAddressIdFromPersonId(long id, Connection con) {

        long addressId = 0;
        try (PreparedStatement ps = con.prepareStatement(QueryStatement.GET_ADDRESS_ID_FROM_PERSON_ID)) {

            ps.setLong(1, id);
            ResultSet resultSet;
            
            if ((resultSet = ps.executeQuery()).next()) {
                addressId = resultSet.getLong(Constant.ADDRESS_ID);
            }
            return addressId;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESSID_FAILED, e);
        }
    }

    public Person preparePerson(ResultSet resultSet) {

        Person person = null;

        try {
            
            person = new Person(resultSet.getString(Constant.FIRST_NAME),
                    resultSet.getString(Constant.LAST_NAME), resultSet.getString(Constant.EMAIL),
                    new java.util.Date(resultSet.getDate(Constant.BIRTH_DATE).getTime()));
            person.setCreatedDate(resultSet.getTimestamp(Constant.CREATED_DATE));
            person.setId(resultSet.getLong(Constant.ID));
            return person;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_PERSON_FAILED, e);
        }
    }

    public void preparePS(PreparedStatement ps, Person person) {

        try {

            ps.setString(1, person.getFirstName());
            ps.setString(2, person.getLastName());
            ps.setString(3, person.getEmail());
            ps.setDate(4, new java.sql.Date(person.getBirthDate().getTime()));
            ps.setLong(5, person.getAddress().getId());
        } catch (Exception e) {
            throw new AppException(ErrorCode.SETTING_VALUE_FAILED, e);
        }
    }

    public boolean getAddressUsage(long addressId, Connection con) {

        boolean usage = true;
        int count;

        try (PreparedStatement ps = con.prepareStatement(QueryStatement.ADDRESS_USAGE)) {

            ps.setLong(1, addressId);
            ResultSet resultSet;
            
            if (!(resultSet = ps.executeQuery()).next()) {
                count = 0;
            }
            count = resultSet.getInt("COUNT(person.id)");

            if (count > 1) {
                usage = true;
            } else if (count == 1) {
                usage = false;
            }
            return usage;
        } catch (Exception e) {
            throw new AppException(ErrorCode.CHECKING_ADDRESS_USAGE_FAILED, e);
        }
    }

    public java.util.Date dateValidator(String date) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date utilDate = null;
        boolean valid = false;

        try {
            LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-uuuu")
                    .withResolverStyle(ResolverStyle.STRICT));

            valid = true;
        } catch (Exception e) {
            valid = false;
            throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
        }

        if (valid == true) {
            try {
                utilDate = formatter.parse(date);
            } catch (Exception e) {
                throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
            }
        }
        return utilDate;
    }

    public void checkNameIsUnique(long id, Person person, Connection con) {

        long personId = 0;
        try (PreparedStatement ps = con.prepareStatement(QueryStatement.NAME_UNIQUE)) {

            ps.setString(1, person.getFirstName());
            ps.setString(2, person.getLastName());
            ResultSet resultSet;
            
            if ((resultSet = ps.executeQuery()).next()) {
                personId = resultSet.getLong(Constant.ID);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.CHECKING_UNIQUE_NAME_FAILED, e);
        }

        if (personId > 0 && personId != id)  {
            throw new AppException(ErrorCode.FIRST_NAME_AND_LAST_NAME_DUPLICATE);
        }
    }
    
    public void checkNameIsUnique(Person person, Connection con) {

        long personId = 0;
        try (PreparedStatement ps = con.prepareStatement(QueryStatement.NAME_UNIQUE)) {

            ps.setString(1, person.getFirstName());
            ps.setString(2, person.getLastName());
            ResultSet resultSet;
            
            if ((resultSet = ps.executeQuery()).next()) {
                personId = resultSet.getLong(Constant.ID);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.CHECKING_UNIQUE_NAME_FAILED, e);
        }

        if (personId > 0)  {
            throw new AppException(ErrorCode.FIRST_NAME_AND_LAST_NAME_DUPLICATE);
        }
    }

    public long assignAddressId(Person person) {

        AddressService addressService = new AddressService();
        long addressId = 0;
        long addressID =
                addressService.getAddressIdForAddress(person.getAddress());

        if (person.getAddress() != null) {

            if (addressID > 0) {
                addressId = addressID;
            } else {
                addressId = addressService.create(person.getAddress());
            }
        }

        return addressId;
    }

    public long assignUpdateAddressId(Person person) {

        AddressService addressService = new AddressService();
        long addressId = 0;

        if (person.getAddress() != null
                && getAddressIdFromPersonId(person.getId(), ConnectionService.get()) == 0) {
            addressId = assignAddressId(person);
        } else if ((person.getAddress() != null
                && getAddressIdFromPersonId(person.getId(), ConnectionService.get()) != 0)) {
            addressId = assignAddressId(person);

            if (getAddressUsage(getAddressIdFromPersonId(person.getId(), ConnectionService.get()),
                    ConnectionService.get()) != true) {
                addressService
                        .delete(getAddressIdFromPersonId(person.getId(), ConnectionService.get()));
            }
        } else if ((person.getAddress() == null
                && getAddressIdFromPersonId(person.getId(), ConnectionService.get()) != 0)) {
            addressId = getAddressIdFromPersonId(person.getId(), ConnectionService.get());
        }

        return addressId;
    }

    public void validatePerson(Person person) {

        if (person.getFirstName() == null || person.getFirstName().equals(" ")) {
            throw new AppException(ErrorCode.INVALID_FIRST_NAME);
        }

        if (person.getLastName() == null || person.getLastName().equals(" ")) {
            throw new AppException(ErrorCode.INVALID_LAST_NAME);
        }

        if (person.getEmail() == null || person.getEmail().equals(" ")) {
            throw new AppException(ErrorCode.INVALID_EMAIL);
        }
    }
    
}
